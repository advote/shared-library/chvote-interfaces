/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.validation.rule.common;

import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.VotingPersonType;
import ch.ge.ve.interfaces.xml.validation.Violation;
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRule;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * {@code StreamedValidationRule} implementation that checks that voters' local person ids are unique across the stream.
 */
public final class Ech45UniqueLocalPersonIdValidationRule implements StreamedValidationRule<VotingPersonType> {

  private final Set<String> uniqueIds  = new HashSet<>();
  private final Set<String> duplicates = new HashSet<>();

  @Override
  public void onElement(VotingPersonType voter) {
    String personId = VoterPersonTypeExtractor.extractFrom(voter)
                                              .getPersonIdentification().getLocalPersonId().getPersonId();
    boolean added = uniqueIds.add(personId);
    if (!added) {
      duplicates.add(personId);
    }
  }

  @Override
  public List<Violation> validate() {
    if (duplicates.isEmpty()) {
      return Collections.emptyList();
    }
    String description = "Contains duplicate localPersonIds: " + duplicates;
    return Collections.singletonList(new Violation(getName(), description));
  }
}
