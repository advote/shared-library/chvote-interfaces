/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.validation.rule.common;

import ch.ge.ve.interfaces.xml.codec.stream.model.CastBallot;
import ch.ge.ve.interfaces.xml.codec.stream.model.Ech222Context;
import ch.ge.ve.interfaces.xml.codec.stream.model.Ech228Context;
import ch.ge.ve.interfaces.xml.codec.stream.model.Ech45Context;
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.VotingPersonType;
import ch.ge.ve.interfaces.xml.ech.eCH0228.v1.VotingCardDeliveryType.VotingCard;
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRule;
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRulesFactory;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Common streamed validation rules factory.
 */
public final class CommonStreamedValidationRulesFactory implements StreamedValidationRulesFactory {

  @Override
  public List<StreamedValidationRule<VotingPersonType>> createEch45ValidationRules(Ech45Context context) {
    return Arrays.asList(
        new Ech45ExtensionValidationRule(),
        new Ech45UniqueLocalPersonIdValidationRule(),
        new Ech45NumberOfVotersValidationRule(context.getNumberOfVoters())
    );
  }

  @Override
  public List<StreamedValidationRule<CastBallot>> createEch222ValidationRules(Ech222Context context) {
    return Collections.emptyList();
  }

  @Override
  public List<StreamedValidationRule<VotingCard>> createEch228ValidationRules(Ech228Context context) {
    return Collections.emptyList();
  }
}
