/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.validation.rule.common;

import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.PersonType;
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.VotingPersonType;

/**
 * Voter person type extractor.
 */
final class VoterPersonTypeExtractor {

  /**
   * Extracts the {@code PersonType} element from the given voter element.
   *
   * @param voter the voter.
   *
   * @return the extracted {@code PersonType}.
   *
   * @throws NullPointerException     if {@code voter} is {@code null}.
   * @throws IllegalArgumentException if no {@code PersonType} could be extracted from the given voter.
   */
  static PersonType extractFrom(VotingPersonType voter) {
    PersonType personType = null;
    if (voter.getPerson() != null) {
      personType = extractFrom(voter.getPerson());
    }
    if (personType == null) {
      throw new IllegalArgumentException("Invalid voter");
    }
    return personType;
  }

  private static PersonType extractFrom(VotingPersonType.Person person) {
    if (person.getSwiss() != null) {
      return person.getSwiss().getSwissDomesticPerson();
    }
    if (person.getForeigner() != null) {
      return person.getForeigner().getForeignerPerson();
    }
    if (person.getSwissAbroad() != null) {
      return person.getSwissAbroad().getSwissAbroadPerson();
    }
    return null;
  }

  private VoterPersonTypeExtractor() {
    throw new AssertionError("Not meant to be instantiated");
  }
}

