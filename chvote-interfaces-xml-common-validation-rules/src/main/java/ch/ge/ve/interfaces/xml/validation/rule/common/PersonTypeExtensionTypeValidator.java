/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.validation.rule.common;

import ch.ge.ve.interfaces.xml.ech.eCH0010.v6.AddressInformationType;
import ch.ge.ve.interfaces.xml.ech.eCH0010.v6.CountryType;
import ch.ge.ve.interfaces.xml.ech.eCH0010.v6.OrganisationMailAddressInfoType;
import ch.ge.ve.interfaces.xml.ech.eCH0010.v6.OrganisationMailAddressType;
import ch.ge.ve.interfaces.xml.ech.extension.PersonTypeExtensionType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

/**
 * {@link PersonTypeExtensionType} validator. Not thread safe.
 */
final class PersonTypeExtensionTypeValidator {

  private final List<String> violations    = new ArrayList<>();
  private final XPath        extensionRoot = XPath.relativeRoot();

  List<String> validate(PersonTypeExtensionType extension) {
    violations.clear();
    mustBePresent(extensionRoot.child("postageCode"), extension.getPostageCode());
    XPath votingPlacePath = extensionRoot.child("votingPlace");
    mustBePresent(votingPlacePath, extension.getVotingPlace(),
                  votingPlace -> validateOrganisationMailAddress(votingPlacePath, votingPlace));
    if (extension.getVotingCardReturnAddress() != null) {
      validateOrganisationMailAddress(extensionRoot.child("votingCardReturnAddress"),
                                      extension.getVotingCardReturnAddress());
    }
    return Collections.unmodifiableList(new ArrayList<>(violations));
  }

  private void validateOrganisationMailAddress(XPath path, OrganisationMailAddressType address) {
    XPath organisationPath = path.child("organisation");
    mustBePresent(organisationPath, address.getOrganisation(),
                  organisation -> validateOrganisationMailAddressInfo(organisationPath, organisation));
    XPath addressInfoPath = path.child("addressInformation");
    mustBePresent(addressInfoPath, address.getAddressInformation(),
                  addressInfo -> validateAddressInfo(addressInfoPath, addressInfo));
  }

  private void validateOrganisationMailAddressInfo(XPath path, OrganisationMailAddressInfoType info) {
    mustBePresent(path.child("organisationName"), info.getOrganisationName());
    maxLengthIfPresent(path.child("organisationName"), info.getOrganisationName(), 60);
    maxLengthIfPresent(path.child("organisationNameAddOn1"), info.getOrganisationNameAddOn1(), 60);
    maxLengthIfPresent(path.child("organisationNameAddOn2"), info.getOrganisationNameAddOn2(), 60);
    mustBeOneOfIfPresent(path.child("mrMrs"), info.getMrMrs(), Arrays.asList("1", "2", "3"));
    maxLengthIfPresent(path.child("title"), info.getTitle(), 50);
    maxLengthIfPresent(path.child("firstName"), info.getFirstName(), 30);
    maxLengthIfPresent(path.child("lastName"), info.getFirstName(), 30);
  }

  private void validateAddressInfo(XPath path, AddressInformationType info) {
    validateAddressLines(path, info);
    validateStreetAndNumber(path, info);
    validatePostOfficeBox(path, info);
    validateLocalityAndTown(path, info);
    validateZipCode(path, info);
    XPath countryPath = path.child("country");
    mustBePresent(countryPath, info.getCountry(), country -> validateCountry(countryPath, country));
  }

  private void validateAddressLines(XPath path, AddressInformationType info) {
    maxLengthIfPresent(path.child("addressLine1"), info.getAddressLine1(), 60);
    maxLengthIfPresent(path.child("addressLine2"), info.getAddressLine2(), 60);
  }

  private void validateStreetAndNumber(XPath path, AddressInformationType info) {
    if (info.getStreet() == null && (info.getHouseNumber() != null || info.getDwellingNumber() != null)) {
      addViolation(path.child("street"), "must be present if houseNumber or dwellingNumber is defined");
    }
    maxLengthIfPresent(path.child("street"), info.getStreet(), 60);
    maxLengthIfPresent(path.child("houseNumber"), info.getHouseNumber(), 12);
    maxLengthIfPresent(path.child("dwellingNumber"), info.getDwellingNumber(), 10);
  }

  private void validatePostOfficeBox(XPath path, AddressInformationType info) {
    if (info.getPostOfficeBoxText() != null && info.getPostOfficeBoxNumber() == null) {
      addViolation(path.child("postOfficeBoxNumber"), "must be present if postOfficeBoxText is defined");
    }
    maxValueIfPresent(path.child("postOfficeBoxNumber"), info.getPostOfficeBoxNumber(), 99999999L);
    maxLengthIfPresent(path.child("postOfficeBoxText"), info.getPostOfficeBoxText(), 15);
  }

  private void validateLocalityAndTown(XPath path, AddressInformationType info) {
    maxLengthIfPresent(path.child("locality"), info.getLocality(), 40);
    maxLengthIfPresent(path.child("town"), info.getTown(), 40);
  }

  private void validateZipCode(XPath path, AddressInformationType info) {
    if (info.getForeignZipCode() != null && info.getSwissZipCode() != null) {
      addViolation(path, "only one of 'swissZipCode' or 'foreignZipCode' can be defined");
    }
    betweenIfPresent(path.child("swissZipCode"), info.getSwissZipCode(), 1000L, 9999L);
    if (info.getSwissZipCodeAddOn() != null || info.getSwissZipCodeId() != null) {
      if (info.getSwissZipCode() == null) {
        addViolation(path.child("swissZipCode"), "must be present if swissZipCodeAddOn or swissZipCodeId is defined");
      }
      maxLengthIfPresent(path.child("swissZipCodeAddOn"), info.getSwissZipCodeAddOn(), 2);
    }
    maxLengthIfPresent(path.child("foreignZipCode"), info.getForeignZipCode(), 40);
  }

  private void validateCountry(XPath path, CountryType country) {
    betweenIfPresent(path.child("countryId"), country.getCountryId(), 1000, 9999);
    if (country.getCountryIdISO2() != null && country.getCountryIdISO2().length() != 2) {
      addViolation(path.child("countryIdISO2"), "must have a length of 2");
    }
    XPath countryNameShortPath = path.child("countryNameShort");
    mustBePresent(countryNameShortPath, country.getCountryNameShort(),
                  countryNameShort -> maxLengthIfPresent(countryNameShortPath, countryNameShort, 50));
  }

  private void addViolation(XPath path, String message) {
    violations.add(path + ": " + message);
  }

  private <E> void mustBePresent(XPath path, E element) {
    mustBePresent(path, element, e -> {/* No op. */});
  }

  private <E> void mustBePresent(XPath path, E element, Consumer<E> ifPresent) {
    if (element == null) {
      addViolation(path, "must be present");
    } else {
      ifPresent.accept(element);
    }
  }

  private void mustBeOneOfIfPresent(XPath path, String value, Collection<String> possibleValues) {
    if (value != null && !possibleValues.contains(value)) {
      addViolation(path, "must be one of " + possibleValues);
    }
  }

  private void maxValueIfPresent(XPath path, Long value, long maxValue) {
    if (value != null && value > maxValue) {
      addViolation(path, "can't be > " + maxValue);
    }
  }

  private void maxLengthIfPresent(XPath path, String value, int maxLength) {
    if (value != null && value.length() > maxLength) {
      addViolation(path, "can't have a length > " + maxLength);
    }
  }

  private void betweenIfPresent(XPath path, Number value, long min, long max) {
    if (value != null) {
      long nb = value.longValue();
      if (nb < min) {
        addViolation(path, "can't be < " + min);
      } else if (nb > max) {
        addViolation(path, "can't be > " + max);
      }
    }
  }

  private static final class XPath {

    static XPath relativeRoot() {
      return new XPath(Collections.singletonList("."));
    }

    private final List<String> elements;

    private XPath(List<String> elements) {
      this.elements = Collections.unmodifiableList(elements);
    }

    XPath child(String element) {
      List<String> child = new ArrayList<>(elements);
      child.add(element);
      return new XPath(child);
    }

    @Override
    public String toString() {
      return String.join("/", elements);
    }
  }
}
