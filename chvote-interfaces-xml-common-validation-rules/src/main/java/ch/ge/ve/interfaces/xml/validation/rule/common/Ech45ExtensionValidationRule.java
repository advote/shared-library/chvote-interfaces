/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.validation.rule.common;

import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.VotingPersonType;
import ch.ge.ve.interfaces.xml.ech.extension.PersonTypeExtensionType;
import ch.ge.ve.interfaces.xml.validation.Violation;
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRule;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * {@code StreamedValidationRule} implementation validating ChVote extensions in eCH-0045 files.
 */
public final class Ech45ExtensionValidationRule implements StreamedValidationRule<VotingPersonType> {

  private final List<Violation> violations = new ArrayList<>();

  @Override
  public void onElement(VotingPersonType element) {
    String personId = VoterPersonTypeExtractor.extractFrom(element)
                                              .getPersonIdentification().getLocalPersonId().getPersonId();
    PersonTypeExtensionType extension = VoterPersonTypeExtractor.extractFrom(element).getExtension();
    if (extension != null) {
      PersonTypeExtensionTypeValidator validator = new PersonTypeExtensionTypeValidator();
      validator.validate(extension).forEach(error -> addViolation(personId, error));
    } else {
      addViolation(personId, "Missing extension");
    }
  }

  @Override
  public List<Violation> validate() {
    return Collections.unmodifiableList(violations);
  }

  private void addViolation(String voterId, String description) {
    violations.add(new Violation(getName(), String.format("[voter %s] %s", voterId, description)));
  }
}
