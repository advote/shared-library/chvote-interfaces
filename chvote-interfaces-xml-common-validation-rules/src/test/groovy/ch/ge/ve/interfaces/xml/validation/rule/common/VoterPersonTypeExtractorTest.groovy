/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.validation.rule.common

import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.ForeignerPersonType
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.ForeignerType
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.SwissAbroadType
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.SwissDomesticType
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.SwissPersonType
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.VotingPersonType
import spock.lang.Specification

class VoterPersonTypeExtractorTest extends Specification {

  static foreigner = foreigner()
  static swissAbroad = swissAbroad()
  static swissDomestic = swissDomestic()

  def "it should extract the correct person type"() {
    expect:
    VoterPersonTypeExtractor.extractFrom(voter) == personType

    where:
    voter         | personType
    foreigner     | foreigner.person.foreigner.foreignerPerson
    swissAbroad   | swissAbroad.person.swissAbroad.swissAbroadPerson
    swissDomestic | swissDomestic.person.swiss.swissDomesticPerson
  }

  def "it should throw a NullPointerException if the given voter is null"() {
    when:
    VoterPersonTypeExtractor.extractFrom((VotingPersonType) null)

    then:
    thrown(NullPointerException)
  }

  def "it should throw an IllegalArgumentException if no PersonType could be extracted from the given voter"() {
    given:
    def invalidVoter1 = new VotingPersonType()
    def invalidVoter2 = new VotingPersonType(person: new VotingPersonType.Person())

    when:
    VoterPersonTypeExtractor.extractFrom(invalidVoter1)

    then:
    thrown(IllegalArgumentException)

    when:
    VoterPersonTypeExtractor.extractFrom(invalidVoter2)

    then:
    thrown(IllegalArgumentException)
  }

  private static VotingPersonType foreigner() {
    def foreigner = new ForeignerType(foreignerPerson: new ForeignerPersonType())
    def person = new VotingPersonType.Person(foreigner: foreigner)
    return new VotingPersonType(person: person)
  }

  private static VotingPersonType swissAbroad() {
    def swissAbroad = new SwissAbroadType(swissAbroadPerson: new SwissPersonType())
    def person = new VotingPersonType.Person(swissAbroad: swissAbroad)
    return new VotingPersonType(person: person)
  }

  private static VotingPersonType swissDomestic() {
    def swissDomestic = new SwissDomesticType(swissDomesticPerson: new SwissPersonType())
    def person = new VotingPersonType.Person(swiss: swissDomestic)
    return new VotingPersonType(person: person)
  }
}
