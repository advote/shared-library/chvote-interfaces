/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.validation.rule.common

import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.VotingPersonType
import spock.lang.Specification

class Ech45NumberOfVotersValidationRuleTest extends Specification {

  def "it must report a violation if the expected number of voters doesn't match the number of elements in the stream"() {
    given:
    def rule = new Ech45NumberOfVotersValidationRule(1)

    when:
    def violations = rule.validate()

    then:
    violations.size() == 1
    violations[0].ruleName == rule.name
    violations[0].description == 'Declared number of voters (1) <> actual number of voters (0)'
  }

  def "it must report no violation if the expected number of voters match the number of elements in the stream"() {
    given:
    def voter = new VotingPersonType()
    def rule = new Ech45NumberOfVotersValidationRule(2)

    when:
    rule.onElement(voter)
    rule.onElement(voter)
    def violations = rule.validate()

    then:
    violations.isEmpty()
  }
}
