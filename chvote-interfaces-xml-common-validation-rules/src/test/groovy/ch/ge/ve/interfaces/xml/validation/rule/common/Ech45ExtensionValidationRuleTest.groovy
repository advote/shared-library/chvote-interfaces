/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.validation.rule.common

import ch.ge.ve.interfaces.xml.ech.eCH0010.v6.AddressInformationType
import ch.ge.ve.interfaces.xml.ech.eCH0010.v6.CountryType
import ch.ge.ve.interfaces.xml.ech.eCH0010.v6.OrganisationMailAddressInfoType
import ch.ge.ve.interfaces.xml.ech.eCH0010.v6.OrganisationMailAddressType
import ch.ge.ve.interfaces.xml.ech.eCH0044.v4.NamedPersonIdType
import ch.ge.ve.interfaces.xml.ech.eCH0044.v4.PersonIdentificationType
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.SwissDomesticType
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.SwissPersonType
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.VotingPersonType
import ch.ge.ve.interfaces.xml.ech.extension.PersonTypeExtensionType
import spock.lang.Specification

class Ech45ExtensionValidationRuleTest extends Specification {

  def "it must report a violation if the extension is missing"() {
    given:
    def rule = new Ech45ExtensionValidationRule()

    when:
    rule.onElement(voterWithExtension("1", null))
    def violations = rule.validate()

    then:
    violations.size() == 1
    violations[0].ruleName == rule.name
    violations[0].description == "[voter 1] Missing extension"
  }

  def "it must report violations if the extension is invalid"() {
    given:
    def rule = new Ech45ExtensionValidationRule()
    def emptyExtension = new PersonTypeExtensionType()

    when:
    rule.onElement(voterWithExtension("1", emptyExtension))
    def violations = rule.validate()

    then:
    violations.size() == 2
    violations[0].ruleName == rule.name
    violations[0].description == "[voter 1] ./postageCode: must be present"
    violations[1].ruleName == rule.name
    violations[1].description == "[voter 1] ./votingPlace: must be present"
  }

  def "it must report no violation if the extension is valid"() {
    given:
    def rule = new Ech45ExtensionValidationRule()

    when:
    rule.onElement(voterWithExtension("1", validExtension()))
    def violations = rule.validate()

    then:
    violations.isEmpty()
  }

  private static VotingPersonType voterWithExtension(String voterId, PersonTypeExtensionType extension) {
    def localPersonId = new NamedPersonIdType(personId: voterId)
    def personIdentification = new PersonIdentificationType(localPersonId: localPersonId)
    def swissPerson = new SwissPersonType(personIdentification: personIdentification, extension: extension)
    def swiss = new SwissDomesticType(swissDomesticPerson: swissPerson)
    def person = new VotingPersonType.Person(swiss: swiss)
    return new VotingPersonType(person: person)
  }

  private static PersonTypeExtensionType validExtension() {
    def organisation = new OrganisationMailAddressInfoType(organisationName: "The Fellowship of the Ring")
    def country = new CountryType(countryNameShort: "Middle-earth")
    def addressInformation = new AddressInformationType(country: country)
    def votingPlace = new OrganisationMailAddressType(addressInformation: addressInformation,
                                                      organisation: organisation)
    return new PersonTypeExtensionType(postageCode: 1, votingPlace: votingPlace)
  }
}
