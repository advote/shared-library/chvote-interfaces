/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.validation.rule.common

import ch.ge.ve.interfaces.xml.ech.eCH0044.v4.NamedPersonIdType
import ch.ge.ve.interfaces.xml.ech.eCH0044.v4.PersonIdentificationType
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.SwissDomesticType
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.SwissPersonType
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.VotingPersonType
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.VotingPersonType.Person
import spock.lang.Specification

class Ech45UniqueLocalPersonIdValidationRuleTest extends Specification {

  def "it must report a violation if there are voters with the same local person id"() {
    given:
    def rule = new Ech45UniqueLocalPersonIdValidationRule()

    when:
    rule.onElement(voter("1"))
    rule.onElement(voter("2"))
    rule.onElement(voter("1"))
    def violations = rule.validate()

    then:
    violations.size() == 1
    violations[0].ruleName == rule.name
    violations[0].description == "Contains duplicate localPersonIds: [1]"
  }

  def "it must report no violation if each voter has a unique local person id"() {
    given:
    def rule = new Ech45UniqueLocalPersonIdValidationRule()

    when:
    rule.onElement(voter("1"))
    rule.onElement(voter("2"))
    rule.onElement(voter("3"))
    def violations = rule.validate()

    then:
    violations.isEmpty()
  }

  private static VotingPersonType voter(String id) {
    def localPersonId = new NamedPersonIdType(personId: id)
    def personIdentification = new PersonIdentificationType(localPersonId: localPersonId)
    def swissPerson = new SwissPersonType(personIdentification: personIdentification)
    def swiss = new SwissDomesticType(swissDomesticPerson: swissPerson)
    def person = new Person(swiss: swiss)
    return new VotingPersonType(person: person)
  }
}
