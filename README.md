# CHVote: Interfaces [![pipeline status](https://gitlab.com/dgsi-ve-private/interfaces/chvote-interfaces/badges/master/pipeline.svg)](https://gitlab.com/dgsi-ve-private/interfaces/chvote-interfaces/commits/master)

A library containing the XSD and java bindings of the eCH standards needed in the CHVote project.

# Usage

## Add to your project

Maven:
```xml
<!-- CHVote interfaces core module -->
<dependency>
    <groupId>ch.ge.ve.interfaces</groupId>
    <artifactId>chvote-interfaces-xml</artifactId>
    <version>2.1.0-SNAPSHOT</version>
</dependency>

<!-- The module containing the common validation rules -->
<dependency>
    <groupId>ch.ge.ve.interfaces</groupId>
    <artifactId>chvote-interfaces-xml-common-validation-rules</artifactId>
    <version>2.1.0-SNAPSHOT</version>
</dependency>
```

For more information refer to: [Usage of chvote-interfaces-xml](chvote-interfaces-xml#usage).

## Command line CHVote XML validation

The [chvote-interfaces-xml-validator](chvote-interfaces-xml-validator) module is a command line tool to validate CHVote
input and output XML files. For more information refer to the [Usage](chvote-interfaces-xml-validator#usage) section of
this module.

# Building

## Pre-requisites

* JDK 8
* Maven

## Build steps

```bash
mvn clean install
```

# Contributing
See [CONTRIBUTING.md](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/CONTRIBUTING.md)

# License
This application is Open Source software released under the [Affero General Public License 3.0](LICENSE) 
license.
