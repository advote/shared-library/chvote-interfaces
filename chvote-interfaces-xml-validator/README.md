chvote-interfaces-xml-validator
===============================

Overview
--------

chvote-interfaces-xml-validator is a command-line application to validate ChVote
platform's input and output XML files.

Usage
-----

First, download and extract the application's distribution archive corresponding
to your system. Then, within the created directory, invoke the `validate` script
followed by the path to the file to validate.

Run `validate -h` to display the built-in help.

The application will detect the type of the given file and validate it against
its schema and against available "business" validation rules.

See `chvote-interfaces-xml` for more information.

Validation rules factories are loaded using Java's `ServiceLoader` facility. By
default, only the `chvote-interfaces-xml-common-validation-rules`' factories
are loaded. To add more validation rules, simply drop the `jar` containing rules
factories implementations into the `lib` directory (in order to be loaded, the
jar must declare the factories it provides).

See [ServiceLoader's documentation][1] for more information.


[1]: https://docs.oracle.com/javase/8/docs/api/java/util/ServiceLoader.html