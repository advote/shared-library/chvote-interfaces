#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

java -cp "lib/*" ch.ge.ve.interfaces.xml.validator.ValidatorCli "$@"