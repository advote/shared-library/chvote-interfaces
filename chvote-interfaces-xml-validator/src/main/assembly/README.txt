                          ${project.name} - version ${project.version}

  What is it?
  -----------

  ${project.name} is a command-line application to validate ChVote XML files.
  It supports validation for the following XML documents against both the corresponding XSD
  and the business-defined consistency rules :
    - eCH-0045
    - eCH-0110
    - eCH-0157
    - eCH-0159
    - eCH-0222
    - eCH-0228
      (see eCH standard : https://www.ech.ch/)
    - counting-circle
    - logistic-delivery
      (ChVote specific formats)

  System Requirements
  -------------------

  ${project.name} is available both for Microsoft Windows and Unix based (eg Linux) OS.
  It should work wherever a proper JRE can be executed, but only the aforementioned
  systems have been tested.

  Java : JRE 1.8 (preferred) or above.

  Installing
  ----------

  1) Unpack the archive and enter the created directory

  2) Make sure your JRE is available in the PATH of your environment,
     ie the command "java -version" returns a proper version of Java

  3) Run "validate <path/to/your/document.xml>" to check that your document is valid

  The application will detect the type / schema of the given file and validate it against
  the XML schema (XSD) and against available "business" validation rules. All violations
  will be printed to the standard output.

  Note: Run "validate -h" to display the built-in help

  Licensing
  ---------

  ${project.name} is distributed under the GNU Affero General Public License (AGPL).
  Please see the file called LICENSE.

  About Java version > 8
  ----------

  Please note that if you are running version 9 or 10 of the JRE :
   1. you should really consider moving to version 11
   2. you have to activate the JAXB API by replacing "java -cp ..." with "java --add-modules java.xml.bind -cp ..."
      in the "validate" command script.

  If you are running JRE 11 (or above), you must provide both JAXB API (javax.xml.bind:jaxb-api) and any implementation
  by placing the corresponding JARs into the "lib/" directory.

