/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.validator;

import ch.ge.ve.interfaces.xml.validation.BusinessValidationException;
import ch.ge.ve.interfaces.xml.validation.SchemaValidationException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Validator common line application.
 */
public final class ValidatorCli {

  private static final Logger logger = LoggerFactory.getLogger(ValidatorCli.class);

  private static final String PROGRAM_NAME = "validate";
  private static final String DISABLE_RULE = "d";
  private static final String HELP         = "h";

  // Disables Sonar's "Either log or rethrow this exception" warning.
  // Some exceptions are purposely not logged or rethrown here...
  // The log messages already capture the context of the failure.
  // There is no need to overflow the console (and the user !) with stack traces.
  @SuppressWarnings("squid:S1166")
  public static void main(String... args) {
    Options options = new Options();
    options.addOption(option(DISABLE_RULE, "disable-rule", "Name of the rule to disable", true));
    options.addOption(option(HELP, "help", "Prints this message", false));
    try {
      CommandLine commandLine = new DefaultParser().parse(options, args, true);
      if (commandLine.hasOption(HELP)) {
        printHelp(options);
        return;
      }
      new Validator().validate(getFileToValidate(commandLine.getArgs()),
                               asList(commandLine.getOptionValues(DISABLE_RULE)));
    } catch (ParseException | IllegalArgumentException e) {
      logger.error("Invalid command line: {}", e.getMessage());
      printHelp(options);
      System.exit(1);
    } catch (UnsupportedOperationException e) {
      logger.warn(e.getMessage());
      System.exit(2);
    } catch (SchemaValidationException e) {
      logger.warn("The file is not XSD-valid: {}", getRootCause(e).getMessage());
      System.exit(2);
    } catch (BusinessValidationException e) {
      logger.warn("The file is XSD-valid, but some validation rules reported violations");
      e.getViolations().forEach(v -> logger.warn(v.toString()));
      System.exit(2);
    } catch (Exception e) {
      logger.error("Unexpected error", e);
      System.exit(-1);
    }
  }

  private static Option option(String name, String longName, String description, boolean hasArg) {
    return Option.builder(name).longOpt(longName).hasArg(hasArg).required(false).desc(description).build();
  }

  private static void printHelp(Options options) {
    HelpFormatter helpFormatter = new HelpFormatter();
    helpFormatter.setWidth(80);
    helpFormatter.printHelp(PROGRAM_NAME + " <file>", "", options, "");
  }

  private static Path getFileToValidate(String... args) {
    if (args.length == 0) {
      throw new IllegalArgumentException("The file to validate must be specified");
    }
    if (args.length > 1) {
      throw new IllegalArgumentException("Too many arguments");
    }
    return Paths.get(args[0]);
  }

  private static List<String> asList(String... strings) {
    return strings == null ? Collections.emptyList() : Arrays.asList(strings);
  }

  private static Throwable getRootCause(Throwable t) {
    Throwable cause = t.getCause();
    return cause == null ? t : getRootCause(cause);
  }
}
