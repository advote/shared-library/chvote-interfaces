/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.validator;

import ch.ge.ve.interfaces.xml.FileType;
import ch.ge.ve.interfaces.xml.XmlSource;
import ch.ge.ve.interfaces.xml.codec.CodecConfiguration;
import ch.ge.ve.interfaces.xml.codec.XmlCodecFactory;
import ch.ge.ve.interfaces.xml.codec.stream.XmlStreamingCodecFactory;
import ch.ge.ve.interfaces.xml.validation.rule.ValidationRulesFactory;
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRulesFactory;
import java.nio.file.Path;
import java.util.List;
import java.util.ServiceLoader;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Validator application's validation logic.
 */
final class Validator {

  private static final Logger logger = LoggerFactory.getLogger(Validator.class);

  void validate(Path file, List<String> disabledRules) {
    XmlSource source = XmlSource.from(file);
    FileType type = getFileType(source);
    logger.info("File type: {}", type);
    if (!disabledRules.isEmpty()) {
      logger.info("Disabled rules: {}", disabledRules);
    }
    CodecConfiguration configuration = new CodecConfiguration().withDisabledValidationRules(disabledRules);
    switch (type) {
      case ECH_0045:
        getXmlStreamingCodecFactory().ech45StreamingCodec(configuration).deserialize(source);
        break;
      case ECH_0071:
        getXmlCodecFactory().ech71Codec().deserialize(source);
        break;
      case ECH_0110:
        getXmlCodecFactory().ech110Codec(configuration).deserialize(source);
        break;
      case ECH_0157:
        getXmlCodecFactory().ech157Codec(configuration).deserialize(source);
        break;
      case ECH_0159:
        getXmlCodecFactory().ech159Codec(configuration).deserialize(source);
        break;
      case ECH_0222:
        getXmlStreamingCodecFactory().ech222StreamingCodec(configuration).deserialize(source);
        break;
      case ECH_0228:
        getXmlStreamingCodecFactory().ech228StreamingCodec(configuration).deserialize(source);
        break;
      case LOGISTIC:
        getXmlCodecFactory().logisticCodec(configuration).deserialize(source);
        break;
      case COUNTING_CIRCLE_LIST:
        getXmlCodecFactory().countingCircleListCodec(configuration).deserialize(source);
        break;
      default:
        throw new UnsupportedOperationException("Unsupported file type: " + type);
    }
    logger.info("The file is valid and no violation has been found");
  }

  private static FileType getFileType(XmlSource source) {
    try {
      return FileType.of(source);
    } catch (IllegalArgumentException e) {
      throw new UnsupportedOperationException("Unsupported file type", e);
    }
  }

  private static XmlCodecFactory getXmlCodecFactory() {
    return new XmlCodecFactory(loadValidationRulesFactories());
  }

  private static XmlStreamingCodecFactory getXmlStreamingCodecFactory() {
    return new XmlStreamingCodecFactory(loadStreamedValidationRulesFactories());
  }

  private static List<ValidationRulesFactory> loadValidationRulesFactories() {
    return StreamSupport.stream(ServiceLoader.load(ValidationRulesFactory.class).spliterator(), false)
                        .peek(factory -> logger.info("Loading {}", factory.getClass().getName()))
                        .collect(Collectors.toList());
  }

  private static List<StreamedValidationRulesFactory> loadStreamedValidationRulesFactories() {
    return StreamSupport.stream(ServiceLoader.load(StreamedValidationRulesFactory.class).spliterator(), false)
                        .peek(factory -> logger.info("Loading {}", factory.getClass().getName()))
                        .collect(Collectors.toList());
  }
}
