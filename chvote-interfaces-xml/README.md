chvote-interfaces-xml
=====================

Overview
--------

`chvote-interfaces-xml` is the core of the `chvote-interfaces` project. It
provides utilities to validate, read and write the various XML files that are
either taken as input or produced as output by the ChVote platform.

It supports the following file types:
  - eCH-0045 4.0
  - eCH-0071 1.1
  - eCH-0110 4.0
  - eCH-0157 4.0
  - eCH-0159 4.0
  - eCH-0222 1.0
  - eCH-0228 1.0
  - logistic 1.0 (ChVote specific)
  - counting circle list 1.0 (ChVote specific)


Design and implementation notes
-------------------------------

Most of the decisions regarding the library's design and implementation are
based on the fact that the set of file formats that need to be supported is
limited and well known (there is no need to provide extension points to support
more formats at runtime).

Also, the set of supported file formats can be clearly split into two
categories. We have eCH-0045, eCH-0222 and eCH-0228 on one hand, and all others
on the other hand. Instances of the first one may be so large that they require
special "constructions" to handle them without overflowing the physical memory:
they need to be processed in streaming.

As a result, the Java classes handling these two categories form two distinct,
independent hierarchies. While keeping them independent, we strived to make
their structure and their usage as similar as possible.


Usage
-----

### Introduction
The main entry points of the library are `XmlCodecFactory` and, its streaming
dual, `XmlStreamingCodecFactory`. As suggested by their name, these classes are
responsible of creating *codecs* for each supported format. A codec's role is
to handle marshalling and unmarshalling of instances of a particular format.
A particular effort has been put in keeping the codecs' APIs as simple as
possible, so that marshalling and unmarshalling are straightforward for most use
cases.

### Configuration options
Codecs may be given a custom configuration at instantiation (*i.e.* at factory
level).

One can for instance specify:
- whether their output must be formatted (boolean, *false* by default)
- whether escaped strings must be put in CDATA sections (boolean, *false* by
  default, except for the ech-0159 codec)
- whether the input/output must be validated (boolean, *true* by default)
- the names of the validation rules to disable (list, *empty* by default)

The first two options should be really straightforward, let's focus on the
validation process...

### Validation
As specified in the previous paragraph, by default, codecs (streaming or not)
validate the content they are processing.

Two kinds of validation is performed by the library. The file/stream is first
validated against its schema. Then, it is validated against more specific
"business" rules. These rules may go beyond the constraints typically found in
schemas and validate the file against rules that are specific to ChVote.

As with codecs, validation rules form two distinct class hierarchies and are
obtained via factories (`ValidationRulesFactory`, and its streaming counterpart,
`StreamedValidationRulesFactory`).

It is possible to programmatically register validation rules factories at codecs
factories instantiation. Some rules may be deactivated by configuration.

Note that the library only specifies the validation framework, and does not
include any validation rule.

### Examples

#### Non-streamed Codecs
- Obtaining an `XmlCodec` with no business validation rules and default configuration _(XSD validation on)_
```java
XmlCodec<Delivery> ech159Codec = new XmlCodecFactory().ech159Codec();
```
- Obtaining an `XmlCodec` with no validation and custom configuration
```java
XmlCodecFactory codecFactory = new XmlCodecFactory();
CodecConfiguration config = new CodecConfiguration().withValidationDisabled()
                                                    .withFormattedOutputEnabled()
                                                    .withEscapeUsingCDataSectionEnabled();
XmlCodec<Delivery> ech159Codec = codecFactory.ech159Codec(configuration);
```
- Obtaining an `XmlCodec` with some disabled validation rules
```java
// "RulesFactory" is defined globally for the "codecFactory"
ValidationRulesFactory rulesFactory = ...;
XmlCodecFactory codecFactory = new XmlCodecFactory(rulesFactory);
// Then each Codec instance can be configured with specific disabled rules
CodecConfiguration config = new CodecConfiguration().withDisabledValidationRules("non-relevant-rule1", "rule2");
XmlCodec<Delivery> ech159Codec = codecFactory.ech159Codec(configuration);
```

#### Streamed Codecs
`XmlStreamingCodec`s allow to read the XML document in a streaming fashion. Configuration is similar as for non-streamed
codecs (same object), but the factories are specific : `StreamedValidationRulesFactory`, `XmlStreamingCodecFactory`.

- Obtaining an `XmlStreamingCodec` with no business validation rules and default configuration
```java
XmlStreamingCodec<VotingCard> ech228Codec = new XmlStreamingCodecFactory().ech228StreamingCodec();
```
- Obtaining an `XmlStreamingCodec` with custom configuration
```java
// "RulesFactory" is defined globally for the "codecFactory"
StreamedValidationRulesFactory rulesFactory = ...;
XmlStreamingCodecFactory codecFactory = new XmlStreamingCodecFactory(rulesFactory);
// Then each Codec instance can be configured with specific configuration
CodecConfiguration config = aPreviousConfiguration.withValidationEnabled()
                                                  .withFormattedOutputEnabled()
                                                  .withEscapeUsingCDataSectionEnabled()
                                                  .withDisabledValidationRules("non-relevant-rule1", "rule2");
XmlStreamingCodec<VotingCard> ech228Codec = codecFactory.ech228StreamingCodec(config);
```