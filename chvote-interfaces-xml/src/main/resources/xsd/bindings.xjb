<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ #%L
  ~ chvote-interfaces
  ~ %%
  ~ Copyright (C) 2016 - 2019 République et Canton de Genève
  ~ %%
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  ~ #L%
  -->

<jaxb:bindings
        xmlns:jaxb="http://java.sun.com/xml/ns/jaxb"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        jaxb:version="2.0"
        xsi:schemaLocation="http://java.sun.com/xml/ns/jaxb http://java.sun.com/xml/ns/jaxb/bindingschema_2_0.xsd">

    <jaxb:globalBindings>
        <jaxb:javaType
                name="java.time.LocalDate"
                xmlType="xs:date"
                parseMethod="ch.ge.ve.interfaces.xml.util.LocalDateAdapter.unmarshal"
                printMethod="ch.ge.ve.interfaces.xml.util.LocalDateAdapter.marshal"/>
        <jaxb:javaType
                name="java.time.LocalDateTime"
                xmlType="xs:dateTime"
                parseMethod="ch.ge.ve.interfaces.xml.util.LocalDateTimeAdapter.unmarshal"
                printMethod="ch.ge.ve.interfaces.xml.util.LocalDateTimeAdapter.marshal"/>
    </jaxb:globalBindings>

    <!-- start with the main target schemas -->

    <!-- eCH-0045 - voter registry -->
    <jaxb:bindings schemaLocation="http://www.ech.ch/xmlns/eCH-0045/4/eCH-0045-4-0.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.ech.eCH0045.v4"/>
        </jaxb:schemaBindings>
        <jaxb:bindings node="//xs:complexType[@name='personType']//xs:element[@name='extension']">
            <jaxb:class ref="ch.ge.ve.interfaces.xml.ech.extension.PersonTypeExtensionType"/>
        </jaxb:bindings>
    </jaxb:bindings>

    <!-- eCH-0071 - municipalities list -->
    <jaxb:bindings schemaLocation="maven:ch.ge.ve.interfaces:chvote-interfaces-schemas:jar::!/xsd/eCH-0071-1-1.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.ech.eCH0071.v1"/>
        </jaxb:schemaBindings>
    </jaxb:bindings>

    <!-- eCH-0159 - votation definition -->
    <jaxb:bindings schemaLocation="maven:ch.ge.ve.interfaces:chvote-interfaces-schemas:jar::!/xsd/eCH-0159-4-0.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.ech.eCH0159.v4"/>
        </jaxb:schemaBindings>
    </jaxb:bindings>

    <!-- eCH-0157 - election definition -->
    <jaxb:bindings schemaLocation="maven:ch.ge.ve.interfaces:chvote-interfaces-schemas:jar::!/xsd/eCH-0157-4-0.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.ech.eCH0157.v4"/>
        </jaxb:schemaBindings>
    </jaxb:bindings>

    <!-- eCH-0110 - votation/election aggregated results -->
    <jaxb:bindings schemaLocation="maven:ch.ge.ve.interfaces:chvote-interfaces-schemas:jar::!/xsd/eCH-0110-4-0.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.ech.eCH0110.v4"/>
        </jaxb:schemaBindings>
    </jaxb:bindings>

    <!-- logistics definition file -->
    <jaxb:bindings
            schemaLocation="maven:ch.ge.ve.interfaces:chvote-interfaces-schemas:jar::!/xsd/logistic-delivery.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.logistic"/>
        </jaxb:schemaBindings>
    </jaxb:bindings>

    <!-- eCH-0228 - printer file -->
    <jaxb:bindings schemaLocation="maven:ch.ge.ve.interfaces:chvote-interfaces-schemas:jar::!/xsd/eCH-0228-1-0.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.ech.eCH0228.v1"/>
        </jaxb:schemaBindings>
        <jaxb:bindings node="//xs:complexType[@name='personType']//xs:element[@name='extension']">
            <jaxb:class ref="ch.ge.ve.interfaces.xml.ech.extension.PersonTypeExtensionType"/>
        </jaxb:bindings>
    </jaxb:bindings>

    <!-- And then, the required dependencies, ordered by ascending eCH number and version -->
    <jaxb:bindings schemaLocation="http://www.ech.ch/xmlns/eCH-0006/2/eCH-0006-2-0.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.ech.eCH0006.v2"/>
        </jaxb:schemaBindings>
    </jaxb:bindings>

    <jaxb:bindings schemaLocation="http://www.ech.ch/xmlns/eCH-0007/5/eCH-0007-5-0.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.ech.eCH0007.v5"/>
        </jaxb:schemaBindings>
    </jaxb:bindings>

    <jaxb:bindings schemaLocation="http://www.ech.ch/xmlns/eCH-0007/6/eCH-0007-6-0.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.ech.eCH0007.v6"/>
        </jaxb:schemaBindings>
    </jaxb:bindings>

    <jaxb:bindings schemaLocation="http://www.ech.ch/xmlns/eCH-0008/3/eCH-0008-3-0.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.ech.eCH0008.v3"/>
        </jaxb:schemaBindings>
    </jaxb:bindings>

    <jaxb:bindings schemaLocation="http://www.ech.ch/xmlns/eCH-0010/5/eCH-0010-5-1.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.ech.eCH0010.v5"/>
        </jaxb:schemaBindings>
    </jaxb:bindings>


    <jaxb:bindings schemaLocation="http://www.ech.ch/xmlns/eCH-0010/6/eCH-0010-6-0.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.ech.eCH0010.v6"/>
        </jaxb:schemaBindings>
    </jaxb:bindings>

    <jaxb:bindings schemaLocation="http://www.ech.ch/xmlns/eCH-0011/8/eCH-0011-8-1.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.ech.eCH0011.v8"/>
        </jaxb:schemaBindings>
    </jaxb:bindings>

    <jaxb:bindings schemaLocation="http://www.ech.ch/xmlns/eCH-0021/7/eCH-0021-7-0.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.ech.eCH0021.v7"/>
        </jaxb:schemaBindings>
    </jaxb:bindings>

    <jaxb:bindings schemaLocation="http://www.ech.ch/xmlns/eCH-0044/4/eCH-0044-4-1.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.ech.eCH0044.v4"/>
        </jaxb:schemaBindings>
    </jaxb:bindings>

    <jaxb:bindings schemaLocation="http://www.ech.ch/xmlns/eCH-0058/5/eCH-0058-5-0.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.ech.eCH0058.v5"/>
        </jaxb:schemaBindings>
    </jaxb:bindings>

    <jaxb:bindings schemaLocation="http://www.ech.ch/xmlns/eCH-0135/1/eCH-0135-1-0.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.ech.eCH0135.v1"/>
        </jaxb:schemaBindings>
    </jaxb:bindings>

    <jaxb:bindings schemaLocation="http://www.ech.ch/xmlns/eCH-0155/4/eCH-0155-4-0.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.ech.eCH0155.v4"/>
        </jaxb:schemaBindings>

        <jaxb:bindings node="//xs:complexType[@name='ballotType']//xs:element[@name='extension']">
            <jaxb:class ref="ch.ge.ve.interfaces.xml.ech.extension.BallotTypeExtensionType"/>
        </jaxb:bindings>
    </jaxb:bindings>

    <jaxb:bindings schemaLocation="http://www.ech.ch/xmlns/eCH-0222/1/eCH-0222-1-0.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.ech.eCH0222.v1"/>
        </jaxb:schemaBindings>
    </jaxb:bindings>

    <jaxb:bindings
            schemaLocation="maven:ch.ge.ve.interfaces:chvote-interfaces-schemas:jar::!/xsd/counting-circle-1-0.xsd">
        <jaxb:schemaBindings>
            <jaxb:package name="ch.ge.ve.interfaces.xml.countingcircle.v1"/>
        </jaxb:schemaBindings>
    </jaxb:bindings>

</jaxb:bindings>
