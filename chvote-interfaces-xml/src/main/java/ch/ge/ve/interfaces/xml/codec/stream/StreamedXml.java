/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream;

import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Represents a streamed XML content.
 *
 * @param <E> the type of the streamed element.
 */
public class StreamedXml<E> {

  private final Supplier<Stream<E>> streamSupplier;

  /**
   * Creates a new {@code StreamedXml}.
   *
   * @param streamSupplier a supplier of the element stream.
   */
  StreamedXml(Supplier<Stream<E>> streamSupplier) {
    this.streamSupplier = Objects.requireNonNull(streamSupplier);
  }

  /**
   * Opens a {@code Stream} of the elements iterated over.
   * <p>
   *   <strong>Notes :</strong> a new stream is opened each time the method is called. <br>
   *   Moreover, each time a stream is successfully created it will (re-)open the underlying XML resource,
   *   and hold the reference until the stream is properly {@link Stream#close closed}.
   *   If timely disposal of said resource is required, the {@code try-with-resources} construct should be used
   *   to ensure that the stream's {@code close()} method is invoked after the stream operations are completed.
   *
   * @return the stream of elements iterated over.
   */
  public Stream<E> stream() {
    return streamSupplier.get();
  }
}
