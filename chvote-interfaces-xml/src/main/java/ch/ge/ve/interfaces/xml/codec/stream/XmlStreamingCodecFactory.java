/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream;

import ch.ge.ve.interfaces.xml.codec.CodecConfiguration;
import ch.ge.ve.interfaces.xml.util.CachingXMLInputFactory;
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRulesFactory;
import java.util.Arrays;
import java.util.Collection;
import javax.xml.stream.XMLInputFactory;

/**
 * {@code XmlStreamingCodec} factory for eCH-0045, eCH-0222 and eCH-0228 files.  Instances of this class as well as
 * {@code XmlStreamingCodec}s they return are thread safe.
 */
public final class XmlStreamingCodecFactory {

  private static final CodecConfiguration DEFAULT_CONFIGURATION = new CodecConfiguration();

  private final StreamedValidationRulesFactory validationRulesFactory;
  private final XMLInputFactory                xmlInputFactory;

  /**
   * Creates a new {@code XmlStreamingCodec}.
   *
   * @param rulesFactories the "business" rules factories.
   *
   * @throws NullPointerException if {@code rulesFactories} is {@code null} of if it contains a {@code null} reference.
   */
  public XmlStreamingCodecFactory(StreamedValidationRulesFactory... rulesFactories) {
    this(Arrays.asList(rulesFactories));
  }

  /**
   * Creates a new {@code XmlStreamingCodec}.
   *
   * @param rulesFactories the "business" rules factories.
   *
   * @throws NullPointerException if {@code rulesFactories} is {@code null} of if it contains a {@code null} reference.
   */
  public XmlStreamingCodecFactory(Collection<StreamedValidationRulesFactory> rulesFactories) {
    this.validationRulesFactory = new AggregatingStreamedValidationRulesFactory(rulesFactories);
    this.xmlInputFactory = CachingXMLInputFactory.getInstance();
  }

  /**
   * Returns an {@code XmlStreamingCodec} instance with a default configuration to read/write eCH-0045 files.
   *
   * @return an {@code XmlStreamingCodec} instance to read/write eCH-0045 files.
   */
  public XmlStreamingCodec<StreamedEch45> ech45StreamingCodec() {
    return ech45StreamingCodec(DEFAULT_CONFIGURATION);
  }

  /**
   * Returns an {@code XmlStreamingCodec} instance to read/write eCH-0045 files.
   *
   * @param configuration custom codec configuration.
   *
   * @return an {@code XmlStreamingCodec} instance to read/write eCH-0045 files.
   *
   * @throws NullPointerException if {@code configuration} is {@code null}.
   */
  public XmlStreamingCodec<StreamedEch45> ech45StreamingCodec(CodecConfiguration configuration) {
    return new Ech45StreamingCodec(xmlInputFactory, configuration, validationRulesFactory);
  }

  /**
   * Returns an {@code XmlStreamingCodec} instance with a default configuration to read/write eCH-0222 files.
   *
   * @return an {@code XmlStreamingCodec} instance to read/write eCH-0222 files.
   */
  public XmlStreamingCodec<StreamedEch222> ech222StreamingCodec() {
    return ech222StreamingCodec(DEFAULT_CONFIGURATION);
  }

  /**
   * Returns an {@code XmlStreamingCodec} instance to read/write eCH-0222 files.
   *
   * @param configuration custom codec configuration.
   *
   * @return an {@code XmlStreamingCodec} instance to read/write eCH-0222 files.
   *
   * @throws NullPointerException if {@code configuration} is {@code null}.
   */
  public XmlStreamingCodec<StreamedEch222> ech222StreamingCodec(CodecConfiguration configuration) {
    return new Ech222StreamingCodec(xmlInputFactory, configuration, validationRulesFactory);
  }

  /**
   * Returns an {@code XmlStreamingCodec} instance with a default configuration to read/write eCH-0228 files.
   *
   * @return an {@code XmlStreamingCodec} instance to read/write eCH-0228 files.
   */
  public XmlStreamingCodec<StreamedEch228> ech228StreamingCodec() {
    return ech228StreamingCodec(DEFAULT_CONFIGURATION);
  }

  /**
   * Returns an {@code XmlStreamingCodec} instance to read/write eCH-0228 files.
   *
   * @param configuration custom codec configuration.
   *
   * @return an {@code XmlStreamingCodec} instance to read/write eCH-0228 files.
   *
   * @throws NullPointerException if {@code configuration} is {@code null}.
   */
  public XmlStreamingCodec<StreamedEch228> ech228StreamingCodec(CodecConfiguration configuration) {
    return new Ech228StreamingCodec(xmlInputFactory, configuration, validationRulesFactory);
  }
}
