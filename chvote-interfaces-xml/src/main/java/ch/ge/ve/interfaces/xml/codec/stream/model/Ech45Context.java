/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream.model;

import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.AuthorityType;
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.ContestType;
import ch.ge.ve.interfaces.xml.ech.eCH0058.v5.HeaderType;
import java.util.Optional;

/**
 * Represents the "context" (the fixed part) of an eCH-0045 file.
 */
public final class Ech45Context {

  private final HeaderType    header;
  private final AuthorityType reportingAuthority;
  private final Long          numberOfVoters;
  private final ContestType   contest;

  public Ech45Context(HeaderType header, AuthorityType reportingAuthority,
                      Long numberOfVoters, ContestType contest) {
    this.header = header;
    this.reportingAuthority = reportingAuthority;
    this.numberOfVoters = numberOfVoters;
    this.contest = contest;
  }

  public HeaderType getHeader() {
    return header;
  }

  public AuthorityType getReportingAuthority() {
    return reportingAuthority;
  }

  public Long getNumberOfVoters() {
    return numberOfVoters;
  }

  public Optional<ContestType> getContest() {
    return Optional.ofNullable(contest);
  }
}
