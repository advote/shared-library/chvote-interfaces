/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JAXB adapter to map date elements to {@code LocalDate} objects.
 */
public final class LocalDateAdapter {

  private static final Logger logger = LoggerFactory.getLogger(LocalDateAdapter.class);

  public static LocalDate unmarshal(String date) {
    try {
      return LocalDate.parse(date, DateTimeFormatter.ISO_DATE);
    } catch (DateTimeParseException ex) {
      logger.error(String.format("Could not parse date: %s", date), ex);
      return null;
    }
  }

  public static String marshal(LocalDate date) {
    return date.format(DateTimeFormatter.ISO_DATE);
  }

  private LocalDateAdapter() {
    throw new AssertionError("Not meant to be instantiated");
  }
}
