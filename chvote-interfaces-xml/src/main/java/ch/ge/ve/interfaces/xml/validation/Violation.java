/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.validation;

import java.io.Serializable;
import java.util.Objects;

/**
 * Holds information regarding a business rule violation. Immutable.
 */
public final class Violation implements Serializable {

  private static final long serialVersionUID = 64793212725954150L;

  private final String ruleName;
  private final String description;

  /**
   * Creates a new {@code Violation}.
   *
   * @param ruleName    the name of the violated rule.
   * @param description the description of the violation.
   *
   * @throws NullPointerException if any of the given arguments is {@code null}.
   */
  public Violation(String ruleName, String description) {
    this.ruleName = Objects.requireNonNull(ruleName);
    this.description = Objects.requireNonNull(description);
  }

  /**
   * Returns the name of the violated rule.
   *
   * @return the name of the violated rule.
   */
  public String getRuleName() {
    return ruleName;
  }

  /**
   * Returns the description of the violation.
   *
   * @return the description of the violation.
   */
  public String getDescription() {
    return description;
  }

  @Override
  public String toString() {
    return ruleName + ": " + description;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Violation violation = (Violation) o;
    return Objects.equals(ruleName, violation.ruleName) &&
           Objects.equals(description, violation.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(ruleName, description);
  }
}
