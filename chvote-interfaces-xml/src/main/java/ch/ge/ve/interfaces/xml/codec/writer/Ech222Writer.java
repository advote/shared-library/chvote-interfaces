/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.writer;

import ch.ge.ve.interfaces.xml.ech.eCH0058.v5.HeaderType;
import ch.ge.ve.interfaces.xml.ech.eCH0222.v1.ElectionRawDataType;
import ch.ge.ve.interfaces.xml.ech.eCH0222.v1.ReportingBodyType;
import ch.ge.ve.interfaces.xml.ech.eCH0222.v1.VoteRawDataType;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import javax.xml.stream.XMLStreamException;

/**
 * XML writer that allows to generate eCH-0222 documents step-by-step.
 * <p>
 *   This is the low-level API to stream-write eCH-0222 documents.
 *   It takes the form of a fluent API flow where each step exposes only methods that are accessible
 *   at this stage of the generation process.
 *   Instances should be obtained from a {@link Ech222WriterFactory} instance.
 * </p>
 * <p>
 *   Note that it is possible to generate XSD-invalid documents (no checks are being made), as well as
 *   bad-structured documents if intermediate objects are being cached and called randomly.
 *   As a low-level API, we consider that the caller is responsible for how he uses it.
 * </p>
 * <p>
 *   {@code Ech222Writer} instances are {@link #close() auto-closeable} in order to free any underlying
 *   resource in a {@code try-with-resource} construct.
 * </p>
 *
 * @see Ech222WriterFactory
 */
public class Ech222Writer implements AutoCloseable {

  private static final String ECH_0222_NS = "http://www.ech.ch/xmlns/eCH-0222/1";

  private static final String DELIVERY                      = "delivery";
  private static final String REPORTING_BODY                = "reportingBody";
  private static final String RAW_DATA_DELIVERY             = "rawDataDelivery";
  private static final String RAW_DATA                      = "rawData";
  private static final String ELECTION_GROUP_RAW_DATA       = "electionGroupBallotRawData";
  private static final String ELECTION_GROUP_IDENTIFICATION = "electionGroupIdentification";
  private static final String CONTEST_IDENTIFICATION        = "contestIdentification";
  private static final String COUNTING_CIRCLE_RAW_DATA      = "countingCircleRawData";
  private static final String COUNTING_CIRCLE_ID            = "countingCircleId";
  private static final String VOTE_RAW_DATA                 = "voteRawData";
  private static final String ELECTION_RAW_DATA             = "electionRawData";
  private static final String VOTE_IDENTIFICATION           = "voteIdentification";
  private static final String ELECTION_IDENTIFICATION       = "electionIdentification";
  private static final String BALLOT_RAW_DATA               = "ballotRawData";

  /**
   * @return an immutable mapping of all namespaces {prefix - uri} used in eCH-0222 documents
   */
  static Map<String, String> getAllNamespaces() {
    Map<String, String> namespaces = new LinkedHashMap<>();
    namespaces.put("e222", ECH_0222_NS);
    namespaces.put("e58", "http://www.ech.ch/xmlns/eCH-0058/5");
    namespaces.put("e8", "http://www.ech.ch/xmlns/eCH-0008/3");
    namespaces.put("e10", "http://www.ech.ch/xmlns/eCH-0010/6");
    namespaces.put("e44", "http://www.ech.ch/xmlns/eCH-0044/4");
    namespaces.put("e155", "http://www.ech.ch/xmlns/eCH-0155/4");
    return Collections.unmodifiableMap(namespaces);
  }

  private final EchWriter writer;
  private final Runnable afterDocumentEndedCallback;

  /**
   * Creates a writer with a callback that would be invoked after the document has been ended,
   * ie after {@link #closeDocument()} has been executed.
   *
   * @param writer a configured instance of {@link EchWriter}
   * @param afterDocumentEndedCallback a callback triggered after the document generation ends
   */
  Ech222Writer(EchWriter writer, Runnable afterDocumentEndedCallback) {
    this.writer = Objects.requireNonNull(writer);
    this.afterDocumentEndedCallback = afterDocumentEndedCallback;
  }

  /**
   * Creates a writer without a callback.
   *
   * @param writer a configured instance of {@link EchWriter}
   */
  Ech222Writer(EchWriter writer) {
    this(writer, null);
  }

  /**
   * Starts the generation of the document with all global information.
   *
   * @param header the content of the {@code deliveryHeader} element
   * @param reportingBody the content of the {@code reportingBody} element
   * @param contestIdentification the value of the {@code contestIdentification} element
   *
   * @return an intermediate object for the fluent API
   */
  public DocumentWriter openDocument(HeaderType header, ReportingBodyType reportingBody, String contestIdentification) {
    writer.openRootElement(ECH_0222_NS, DELIVERY);
    writer.writeDeliveryHeader(ECH_0222_NS, header);

    writer.writeStartElement(ECH_0222_NS, RAW_DATA_DELIVERY);
    writer.writeObject(ECH_0222_NS, REPORTING_BODY, reportingBody, ReportingBodyType.class);

    writer.writeStartElement(ECH_0222_NS, RAW_DATA);
    writer.writeElement(ECH_0222_NS, CONTEST_IDENTIFICATION, contestIdentification);
    return new DocumentWriter();
  }

  /**
   * Generate ending tags from the global context and flushes the result to the output if necessary.
   * <p>
   *   If a callback has been configured, it will be invoked after the ending root element tag has been added
   *   to the document (and after it has been flushed to the output if applicable).
   * </p>
   */
  public void closeDocument() {
    writer.writeEndElement(ECH_0222_NS, RAW_DATA);
    writer.writeEndElement(ECH_0222_NS, RAW_DATA_DELIVERY);
    writer.endRootElement(ECH_0222_NS, DELIVERY);

    if (afterDocumentEndedCallback != null) {
      afterDocumentEndedCallback.run();
    }
  }

  /**
   * Frees any underlying resource by calling the close method on the xml writer encapsulated
   * in our {@code EchWriter} instance.
   *
   * @throws XMLStreamException if raised by the xml writer
   */
  @Override
  public void close() throws XMLStreamException {
    writer.getXmlEventWriter().close();
  }

  /**
   * Intermediate class to give access to counting circle section writers.
   */
  public final class DocumentWriter {

    /**
     * Starts a new {@code <countingCircleRawData>} section.
     *
     * @param countingCircleId value for the {@code countingCircleId} element
     *
     * @return a new writer dedicated to counting circle sections
     */
    public CountingCircleSectionWriter createCountingCircle(String countingCircleId) {
      return new CountingCircleSectionWriter(writer, this).openSection(countingCircleId);
    }

    /**
     * Cascade-ends the document and closes it.
     *
     * @see Ech222Writer#closeDocument()
     */
    public void endDocument() {
      Ech222Writer.this.closeDocument();
    }

  }

  /**
   * Intermediate class to give access to counting circle section's content writers : vote or election sections.
   */
  public static final class CountingCircleSectionWriter {

    private final EchWriter writer;
    private final DocumentWriter parent;

    private CountingCircleSectionWriter(EchWriter writer, DocumentWriter parent) {
      this.writer = writer;
      this.parent = parent;
    }

    private CountingCircleSectionWriter openSection(String countingCircleId) {
      writer.writeStartElement(ECH_0222_NS, COUNTING_CIRCLE_RAW_DATA);
      writer.writeElement(ECH_0222_NS, COUNTING_CIRCLE_ID, countingCircleId);
      return this;
    }

    /**
     * Creates a sibling {@code <countingCircleRawData>} section. The current section is closed beforehand.
     *
     * @see #endCountingCircleSection()
     * @see DocumentWriter#createCountingCircle(String)
     */
    public CountingCircleSectionWriter createNextCountingCircle(String countingCircleId) {
      this.endCountingCircleSection();
      return this.openSection(countingCircleId);
    }

    /**
     * Starts a new {@code <voteRawData>} section.
     *
     * @param voteIdentification value for the {@code voteIdentification} element
     *
     * @return a new writer dedicated to vote sections
     */
    public VoteSectionWriter createVoteRawData(String voteIdentification) {
      return new VoteSectionWriter(writer, this).openSection(voteIdentification);
    }

    /**
     * Starts a new {@code <electionGroupBallotRawData>} section with no {@code electionGroupIdentification} element.
     *
     * @return a new writer dedicated to election group sections
     */
    public ElectionGroupSectionWriter createElectionGroupBallotRawData() {
      return createElectionGroupBallotRawData(null);
    }

    /**
     * Starts a new {@code <electionGroupBallotRawData>} section.
     *
     * @param electionGroupIdentification value for the {@code electionGroupIdentification} element
     *
     * @return a new writer dedicated to election group sections
     */
    public ElectionGroupSectionWriter createElectionGroupBallotRawData(String electionGroupIdentification) {
      return new ElectionGroupSectionWriter(writer, this).openSection(electionGroupIdentification);
    }

    /**
     * Ends the current {@code <countingCircleRawData>} section.
     */
    public DocumentWriter endCountingCircleSection() {
      writer.writeEndElement(ECH_0222_NS, COUNTING_CIRCLE_RAW_DATA);
      return parent;
    }

    /**
     * Cascade-ends the document and closes it.
     *
     * @see Ech222Writer#closeDocument()
     */
    public void endDocument() {
      this.endCountingCircleSection();
      parent.endDocument();
    }
  }

  /**
   * Intermediate class to give access to vote section's content writers.
   */
  public static final class VoteSectionWriter {

    private final EchWriter writer;
    private final CountingCircleSectionWriter parent;

    private VoteSectionWriter(EchWriter writer, CountingCircleSectionWriter parent) {
      this.writer = writer;
      this.parent = parent;
    }

    private VoteSectionWriter openSection(String voteIdentification) {
      writer.writeStartElement(ECH_0222_NS, VOTE_RAW_DATA);
      writer.writeElement(ECH_0222_NS, VOTE_IDENTIFICATION, voteIdentification);
      return this;
    }

    /**
     * Creates a sibling {@code <voteRawData>} section. The current section is closed beforehand.
     *
     * @see #endVoteRawDataSection()
     * @see CountingCircleSectionWriter#createVoteRawData(String)
     */
    public VoteSectionWriter createNextVoteRawData(String voteIdentification) {
      this.endVoteRawDataSection();
      return this.openSection(voteIdentification);
    }

    /**
     * Appends a {@code BallotRawData} element : this is the JAXB bean representing a cast ballot for a vote.
     * There can be (and is expected to be) multiple elements for a vote section.
     *
     * @param castBallot content of the {@code ballotRawData} element
     */
    public VoteSectionWriter appendCastBallot(VoteRawDataType.BallotRawData castBallot) {
      writer.writeObject(ECH_0222_NS, BALLOT_RAW_DATA, castBallot, VoteRawDataType.BallotRawData.class);
      return this;
    }

    /**
     * Ends the current {@code <voteRawData>} section.
     */
    public CountingCircleSectionWriter endVoteRawDataSection() {
      writer.writeEndElement(ECH_0222_NS, VOTE_RAW_DATA);
      return parent;
    }

    /**
     * Cascade-ends the document and closes it.
     *
     * @see Ech222Writer#closeDocument()
     */
    public void endDocument() {
      this.endVoteRawDataSection();
      parent.endDocument();
    }
  }


  /**
   * Intermediate class to give access to election group section's content writers.
   */
  public static final class ElectionGroupSectionWriter {

    private final EchWriter writer;
    private final CountingCircleSectionWriter parent;

    private ElectionGroupSectionWriter(EchWriter writer, CountingCircleSectionWriter parent) {
      this.writer = writer;
      this.parent = parent;
    }

    private ElectionGroupSectionWriter openSection(String electionGroupIdentification) {
      writer.writeStartElement(ECH_0222_NS, ELECTION_GROUP_RAW_DATA);
      if (electionGroupIdentification != null) {
        writer.writeElement(ECH_0222_NS, ELECTION_GROUP_IDENTIFICATION, electionGroupIdentification);
      }
      return this;
    }

    /**
     * Creates a sibling {@code <electionGroupBallotRawData>} section. The current section is closed beforehand.
     *
     * @see #endElectionGroupBallotRawDataSection()
     * @see CountingCircleSectionWriter#createElectionGroupBallotRawData()
     */
    public ElectionGroupSectionWriter createNextElectionGroupBallotRawData() {
      return this.createNextElectionGroupBallotRawData(null);
    }

    /**
     * Creates a sibling {@code <electionGroupBallotRawData>} section. The current section is closed beforehand.
     *
     * @see #endElectionGroupBallotRawDataSection()
     * @see CountingCircleSectionWriter#createElectionGroupBallotRawData(String)
     */
    public ElectionGroupSectionWriter createNextElectionGroupBallotRawData(String electionGroupIdentification) {
      this.endElectionGroupBallotRawDataSection();
      return this.openSection(electionGroupIdentification);
    }

    /**
     * Starts a new {@code <electionRawData>} section.
     *
     * @param electionIdentification value for the {@code electionIdentification} element
     *
     * @return a new writer dedicated to election sections
     */
    public ElectionSectionWriter createElectionRawData(String electionIdentification) {
      return new ElectionSectionWriter(writer, this).openSection(electionIdentification);
    }

    /**
     * Ends the current {@code <electionGroupBallotRawData>} section.
     */
    public CountingCircleSectionWriter endElectionGroupBallotRawDataSection() {
      writer.writeEndElement(ECH_0222_NS, ELECTION_GROUP_RAW_DATA);
      return parent;
    }

    /**
     * Cascade-ends the document and closes it.
     *
     * @see Ech222Writer#closeDocument()
     */
    public void endDocument() {
      this.endElectionGroupBallotRawDataSection();
      parent.endDocument();
    }
  }

  /**
   * Intermediate class to give access to election section's content writers.
   */
  public static final class ElectionSectionWriter {

    private final EchWriter writer;
    private final ElectionGroupSectionWriter parent;

    private ElectionSectionWriter(EchWriter writer, ElectionGroupSectionWriter parent) {
      this.writer = writer;
      this.parent = parent;
    }

    private ElectionSectionWriter openSection(String electionIdentification) {
      writer.writeStartElement(ECH_0222_NS, ELECTION_RAW_DATA);
      writer.writeElement(ECH_0222_NS, ELECTION_IDENTIFICATION, electionIdentification);
      return this;
    }

    /**
     * Creates a sibling {@code <electionRawData>} section. The current section is closed beforehand.
     *
     * @see #endElectionRawDataSection()
     * @see ElectionGroupSectionWriter#createElectionRawData(String)
     */
    public ElectionSectionWriter createNextElectionRawData(String electionIdentification) {
      this.endElectionRawDataSection();
      return this.openSection(electionIdentification);
    }

    /**
     * Appends a {@code BallotRawData} element : this is the JAXB bean representing a cast ballot for an election.
     * There can be (and is expected to be) multiple elements for an election section.
     *
     * @param electionBallot content of the {@code electionBallot} element
     */
    public ElectionSectionWriter appendCastBallot(ElectionRawDataType.BallotRawData electionBallot) {
      writer.writeObject(ECH_0222_NS, BALLOT_RAW_DATA, electionBallot, ElectionRawDataType.BallotRawData.class);
      return this;
    }

    /**
     * Ends the current {@code <electionRawData>} section.
     */
    public ElectionGroupSectionWriter endElectionRawDataSection() {
      writer.writeEndElement(ECH_0222_NS, ELECTION_RAW_DATA);
      return parent;
    }

    /**
     * Cascade-ends the document and closes it.
     *
     * @see Ech222Writer#closeDocument()
     */
    public void endDocument() {
      this.endElectionRawDataSection();
      parent.endDocument();
    }
  }

}
