/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream;

import ch.ge.ve.interfaces.xml.XmlSource;
import ch.ge.ve.interfaces.xml.codec.CodecConfiguration;
import ch.ge.ve.interfaces.xml.codec.XmlDeserializationException;
import ch.ge.ve.interfaces.xml.codec.stream.model.Ech228Context;
import ch.ge.ve.interfaces.xml.ech.eCH0058.v5.HeaderType;
import ch.ge.ve.interfaces.xml.ech.eCH0228.v1.Delivery;
import ch.ge.ve.interfaces.xml.ech.eCH0228.v1.NamedCodeType;
import ch.ge.ve.interfaces.xml.ech.eCH0228.v1.VotingCardDeliveryType.ContestData;
import ch.ge.ve.interfaces.xml.ech.eCH0228.v1.VotingCardDeliveryType.VotingCard;
import ch.ge.ve.interfaces.xml.util.CachingJAXBContextFactory;
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRule;
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRulesFactory;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Stream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;

/**
 * {@code XmlStreamingCodec} implementation for the eCH-0228 format.
 */
final class Ech228StreamingCodec extends AbstractStreamingCodec<VotingCard, StreamedEch228> {

  private static final String VOTING_CARD_TAG_NAME     = "votingCard";
  private static final String DELIVERY_HEADER_TAG_NAME = "deliveryHeader";
  private static final String CONTEST_DATA_TAG_NAME    = "contestData";
  private static final String LOGISTIC_CODE_TAG_NAME   = "logisticCode";

  private final JAXBContext                    context;
  private final XMLInputFactory                xmlInputFactory;
  private final StreamedValidationRulesFactory rulesFactory;

  /**
   * Creates a new {@code Ech228StreamingCodec}.
   *
   * @param xmlInputFactory the XML input factory to use.
   * @param configuration   the codec configuration.
   * @param rulesFactory    the "business" rules factory.
   *
   * @throws NullPointerException if any of the given arguments is {@code null}.
   */
  Ech228StreamingCodec(XMLInputFactory xmlInputFactory, CodecConfiguration configuration,
                       StreamedValidationRulesFactory rulesFactory) {
    super(configuration);
    this.context = CachingJAXBContextFactory.getJaxbContextFor(Delivery.class);
    this.xmlInputFactory = Objects.requireNonNull(xmlInputFactory);
    this.rulesFactory = Objects.requireNonNull(rulesFactory);
  }

  @Override
  protected StreamedEch228 createStreamedXmlObject(XmlSource source) {
    return new StreamedEch228(readContext(source), votingCardsSupplier(source));
  }

  private Ech228Context readContext(XmlSource source) {
    return source.read(inputStream -> {
      try {
        ContextBuilder builder = new ContextBuilder();
        XmlElementConsumerRegistry registry = new XmlElementConsumerRegistry(context.createUnmarshaller());
        registry.registerConsumer(DELIVERY_HEADER_TAG_NAME, HeaderType.class, builder::setHeader);
        registry.registerConsumer(CONTEST_DATA_TAG_NAME, ContestData.class, builder::setContestData);
        XmlStreamWalker walker = new XmlStreamWalker(registry, xmlInputFactory.createXMLStreamReader(inputStream));
        walker.walkUntil(XmlElementMatcher.startOf(VOTING_CARD_TAG_NAME));
        builder.setLogisticCodesSupplier(logisticCodesSupplier(source));
        return builder.build();
      } catch (XMLStreamException | JAXBException e) {
        throw new XmlDeserializationException("Deserialization failed", e);
      }
    });
  }

  private Supplier<Stream<NamedCodeType>> logisticCodesSupplier(XmlSource source) {
    ElementsExtractor extractor = new ElementsExtractor(xmlInputFactory, context, source);
    return () -> extractor.extract(LOGISTIC_CODE_TAG_NAME, NamedCodeType.class);
  }

  private Supplier<Stream<VotingCard>> votingCardsSupplier(XmlSource source) {
    ElementsExtractor extractor = new ElementsExtractor(xmlInputFactory, context, source);
    return () -> extractor.extract(VOTING_CARD_TAG_NAME, VotingCard.class);
  }

  @Override
  protected Stream<StreamedValidationRule<VotingCard>> allExistingRules(StreamedEch228 ech228) {
    return rulesFactory.createEch228ValidationRules(ech228.getContext()).stream();
  }

  private static final class ContextBuilder {

    private HeaderType                      header;
    private ContestData                     contestData;
    private Supplier<Stream<NamedCodeType>> logisticCodesSupplier;

    void setHeader(HeaderType header) {
      this.header = header;
    }

    void setContestData(ContestData contestData) {
      this.contestData = contestData;
    }

    void setLogisticCodesSupplier(Supplier<Stream<NamedCodeType>> logisticCodesSupplier) {
      this.logisticCodesSupplier = logisticCodesSupplier;
    }

    Ech228Context build() {
      return new Ech228Context(header, contestData, logisticCodesSupplier);
    }
  }
}
