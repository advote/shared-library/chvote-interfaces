/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.ech.extension;

import ch.ge.ve.interfaces.xml.ech.eCH0010.v6.OrganisationMailAddressType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Java class for personTypeExtensionType type.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "personTypeExtensionType", propOrder = {
    "postageCode",
    "votingPlace",
    "votingCardReturnAddress"
})
public class PersonTypeExtensionType {

  @XmlElement(required = true)
  private Integer postageCode;

  @XmlElement(required = true)
  private OrganisationMailAddressType votingPlace;

  @XmlElement(required = false)
  private OrganisationMailAddressType votingCardReturnAddress;

  public Integer getPostageCode() {
    return postageCode;
  }

  public void setPostageCode(Integer postageCode) {
    this.postageCode = postageCode;
  }

  public OrganisationMailAddressType getVotingPlace() {
    return votingPlace;
  }

  public void setVotingPlace(OrganisationMailAddressType votingPlace) {
    this.votingPlace = votingPlace;
  }

  public OrganisationMailAddressType getVotingCardReturnAddress() {
    return votingCardReturnAddress;
  }

  public void setVotingCardReturnAddress(OrganisationMailAddressType votingCardReturnAddress) {
    this.votingCardReturnAddress = votingCardReturnAddress;
  }
}
