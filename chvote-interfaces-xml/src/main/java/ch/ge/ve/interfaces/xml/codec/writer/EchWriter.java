/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.writer;

import ch.ge.ve.interfaces.xml.codec.XmlSerializationException;
import ch.ge.ve.interfaces.xml.ech.eCH0058.v5.HeaderType;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

/**
 * A helper class that encapsulate a Marshaller and the XMLEventWriter to expose basic element writing control.
 * <p>
 *   Generally speaking, the implementation / configuration of all used {@code javax.xml} objects is
 *   left to the user so that he can fine-tune the XML generation process.
 *   The primary goal of this class is to facilitate the generation of XML elements as they are used
 *   in the "eCH" standard.
 * </p>
 */
final class EchWriter {

  private final Marshaller marshaller;

  private final XMLEventFactory eventFactory;
  private final XMLEventWriter  xmlEventWriter;

  private final Set<String> namespaceURIs;

  /**
   * Instantiates an {@code EchWriter} to write an XML document.
   * <p>
   *   The caller is responsible to create both the marshaller with the appropriate context and
   *   the {@code XMLEventWriter} that controls the output. This means he is also responsible for
   *   closing any allocated resource.
   * </p>
   * <p>
   *   As the given marshaller is used to marshall sub-elements rather than the whole document root,
   *   its JAXB_FRAGMENT property will be forced to {@code true} by this constructor.
   * </p>
   *
   * @param marshaller a JAXB marshaller that allow to write whole objects to the XML output
   * @param eventFactory a factory for creating XML events - might be declared once and reused by the caller
   * @param xmlEventWriter the XML writer responsible to handle created XML events
   * @param namespaces the {prefix - URI} mapping of all namespaces that will be used in the document
   */
  EchWriter(Marshaller marshaller, XMLEventFactory eventFactory, XMLEventWriter xmlEventWriter,
            Map<String, String> namespaces) {
    this.marshaller = marshaller;
    this.eventFactory = eventFactory;
    this.xmlEventWriter = xmlEventWriter;
    this.namespaceURIs = Collections.unmodifiableSet(new LinkedHashSet<>(namespaces.values()));

    try {
      for (Map.Entry<String, String> namespace : namespaces.entrySet()) {
        // binds prefix - URI in the context of the ROOT_ELEMENT > whole document
        this.xmlEventWriter.setPrefix(namespace.getKey(), namespace.getValue());
      }

      this.marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
    } catch (Exception e) {
      throw new XmlSerializationException("Failed to initialize JAXB marshaller", e);
    }
  }

  XMLEventWriter getXmlEventWriter() {
    return xmlEventWriter;
  }

  private String getPrefixFor(String namespaceURI) {
    return Objects.requireNonNull(xmlEventWriter.getNamespaceContext().getPrefix(namespaceURI),
                                  () -> "No prefix found for \"" + namespaceURI + "\". Has the namespace been defined ?");
  }

  /**
   * Starts generating the XML document.
   * <p>
   *   This method generates the XML prolog and the opening root element.
   *   All namespaces defined at {@code EchWriter}'s instantiation are declared within the root tag.
   * </p>
   *
   * @param rootNamespaceURI namespace containing the root element
   * @param rootTag root element tag name
   */
  public void openRootElement(String rootNamespaceURI, String rootTag) {
    try {
      final String encoding = Objects.toString(marshaller.getProperty(Marshaller.JAXB_ENCODING), "UTF-8");
      xmlEventWriter.add(eventFactory.createStartDocument(encoding, "1.0"));
      writeStartElement(rootNamespaceURI, rootTag);

      // writes namespaces
      for (String uri : namespaceURIs) {
        xmlEventWriter.add(eventFactory.createNamespace(getPrefixFor(uri), uri));
      }
    } catch (Exception e) {
      throw new XmlSerializationException(
          String.format("Failed to write root element \"%s\" with namespace \"%s\"", rootTag, rootNamespaceURI), e);
    }
  }

  /**
   * Generates the {@code <deliveryHeader>} standard eCH-0058 element from the JAXB object
   *
   * @param namespaceURI namespace owning the "deliveryHeader" element declaration
   * @param headers the JAXB object with the header's content
   */
  public void writeDeliveryHeader(String namespaceURI, HeaderType headers) {
    writeObject(namespaceURI, "deliveryHeader", headers, HeaderType.class);
  }

  /**
   * Generates a starting element tag with no attributes.
   *
   * @param namespaceURI namespace owning the element
   * @param localName local name (un-prefixed) of the element
   */
  public void writeStartElement(String namespaceURI, String localName) {
    try {
      xmlEventWriter.add(eventFactory.createStartElement(getPrefixFor(namespaceURI), namespaceURI, localName));
    } catch (XMLStreamException e) {
      throw new XmlSerializationException(
          String.format("Failed to write start of element \"%s\" with namespace \"%s\"", localName, namespaceURI), e);
    }
  }

  /**
   * Generates an ending element tag.
   * <p>
   *   No check is made that the tag has actually been opened, and no guarantee as what happens if there is
   *   no corresponding start element. The result could vary depending on the {@code javax.xml} implementation used.
   * </p>
   *
   * @param namespaceURI namespace owning the element
   * @param localName local name (un-prefixed) of the element
   */
  public void writeEndElement(String namespaceURI, String localName) {
    try {
      xmlEventWriter.add(eventFactory.createEndElement(getPrefixFor(namespaceURI), namespaceURI, localName));
    } catch (XMLStreamException e) {
      throw new XmlSerializationException(
          String.format("Failed to write end of element \"%s\" with namespace \"%s\"", localName, namespaceURI), e);
    }
  }

  /**
   * Generates the closing root element tag.
   * <p>
   *   No check is made that all opened elements have been closed, and no guarantee as what happens if there still
   *   are opened elements. The result could vary depending on the {@code javax.xml} implementation used.
   * </p>
   *
   * @param rootNamespaceURI namespace containing the root element
   * @param rootTag root element tag name
   */
  public void endRootElement(String rootNamespaceURI, String rootTag) {
    writeEndElement(rootNamespaceURI, rootTag);
    try {
      xmlEventWriter.add(eventFactory.createEndDocument());
      xmlEventWriter.flush();
    } catch (XMLStreamException e) {
      throw new XmlSerializationException("Failed to write the end of the document", e);
    }
  }

  /**
   * Generates an entire simple element, that is : start tag, string content and end tag.
   *
   * @param namespaceURI namespace owning the element
   * @param elementName local name (un-prefixed) of the element
   * @param content the content of the element, formatted as a string
   */
  public void writeElement(String namespaceURI, String elementName, String content) {
    String prefix = getPrefixFor(namespaceURI);
    try {
      xmlEventWriter.add(eventFactory.createStartElement(prefix, namespaceURI, elementName));
      xmlEventWriter.add(eventFactory.createCharacters(content));
      xmlEventWriter.add(eventFactory.createEndElement(prefix, namespaceURI, elementName));
    } catch (XMLStreamException e) {
      throw new XmlSerializationException(
          String.format("Failed to write element \"%s\" with namespace \"%s\"", elementName, namespaceURI), e);
    }
  }

  /**
   * Generates an element using a JAXB object to define its content.
   * <p>
   *   The JAXB marshaller this object has been instantiated with must be capable of marshalling the given object
   *   (<i>ie</i> the object type must be included in the marshaller's JAXB context).
   * </p>
   *
   * @param namespaceURI namespace owning the container element
   * @param elementName local name (un-prefixed) of the container element
   * @param content JAXB object holding the content of the element
   * @param contentType the JAXB class that defines the XML binding (could be a superclass of the content object)
   */
  public <T> void writeObject(String namespaceURI, String elementName, T content, Class<T> contentType) {
    try {
      marshaller.marshal(new JAXBElement<>(new QName(namespaceURI, elementName), contentType, content), xmlEventWriter);
    } catch (JAXBException e) {
      String message = String.format("Failed to write the element \"%s\" with namespace \"%s\" with content of type %s",
                                     elementName, namespaceURI, contentType.getName());
      throw new XmlSerializationException(message, e);
    }
  }

}
