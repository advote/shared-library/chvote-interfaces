/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.validation.rule.stream;

import ch.ge.ve.interfaces.xml.validation.Violation;
import java.util.List;

/**
 * Base representation of a streamed validation rule. Note that validation rules are only invoked if validation is
 * enabled and the read/generated XML is XSD-valid. Implementations are required to be thread safe.
 *
 * @param <E> the type of the element iterated over.
 */
public interface StreamedValidationRule<E> {

  /**
   * Returns the rule's name (defaults to the class name).
   *
   * @return the rule's name.
   */
  default String getName() {
    return getClass().getName();
  }

  /**
   * This method gets called once for each element in the stream.
   *
   * @param element the current element in the stream.
   */
  void onElement(E element);

  /**
   * Performs the actual validation and return all found violations.
   *
   * @return all found violations.
   */
  List<Violation> validate();
}
