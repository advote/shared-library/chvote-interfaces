/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream;

import ch.ge.ve.interfaces.xml.codec.XmlDeserializationException;
import java.util.function.Predicate;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * "Walks" down an XML stream, invoking predefined consumers on matching elements. Not thread safe.
 */
final class XmlStreamWalker {

  private final XmlElementConsumerRegistry consumersRegistry;
  private final XMLStreamReader            reader;

  XmlStreamWalker(XmlElementConsumerRegistry consumersRegistry, XMLStreamReader reader) {
    this.consumersRegistry = consumersRegistry;
    this.reader = reader;
  }

  public void walk() {
    walkUntil(streamReader -> false);
  }

  public void walkUntil(XmlElementMatcher endElementMatcher) {
    walkUntil(endElementMatcher::matchesCurrentPosition);
  }

  private void walkUntil(Predicate<XMLStreamReader> stopPredicate) {
    try {
      do {
        if (hasMoreElements()) {
          consumeCurrentElement();
        }
      } while (reader.hasNext() && !stopPredicate.test(reader));
    } catch (XMLStreamException e) {
      throw new XmlDeserializationException("Failed while walking stream", e);
    }
  }

  private boolean hasMoreElements() throws XMLStreamException {
    ensureIsOnStartElement();
    return reader.hasNext();
  }

  private void ensureIsOnStartElement() throws XMLStreamException {
    while (reader.hasNext() && reader.getEventType() != XMLStreamConstants.START_ELEMENT) {
      reader.next();
    }
  }

  private void consumeCurrentElement() throws XMLStreamException {
    boolean consumed = consumersRegistry.consumeCurrentElement(reader);
    if (!consumed) {
      reader.next();
    }
    ensureIsOnStartElement();
  }
}
