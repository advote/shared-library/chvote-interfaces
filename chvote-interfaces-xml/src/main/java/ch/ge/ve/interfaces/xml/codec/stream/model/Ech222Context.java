/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream.model;

import ch.ge.ve.interfaces.xml.ech.eCH0058.v5.HeaderType;
import ch.ge.ve.interfaces.xml.ech.eCH0222.v1.ReportingBodyType;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Represents the "context" (the fixed part) of an eCH-0222 file.
 */
public final class Ech222Context {

  private final HeaderType               header;
  private final ReportingBodyType        reportingBody;
  private final String                   contestIdentification;
  private final Supplier<Stream<String>> countingCircleIdsSupplier;

  public Ech222Context(HeaderType header, ReportingBodyType reportingBody,
                       String contestIdentification, Supplier<Stream<String>> countingCircleIds) {
    this.header = header;
    this.reportingBody = reportingBody;
    this.contestIdentification = contestIdentification;
    this.countingCircleIdsSupplier = countingCircleIds;
  }

  public HeaderType getHeader() {
    return header;
  }

  public ReportingBodyType getReportingBody() {
    return reportingBody;
  }

  public String getContestIdentification() {
    return contestIdentification;
  }

  public Stream<String> getCountingCircleIds() {
    return countingCircleIdsSupplier.get();
  }
}
