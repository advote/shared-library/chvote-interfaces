/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream;

import ch.ge.ve.interfaces.xml.XmlSource;
import ch.ge.ve.interfaces.xml.codec.CodecConfiguration;
import ch.ge.ve.interfaces.xml.codec.XmlDeserializationException;
import ch.ge.ve.interfaces.xml.codec.stream.model.CastBallot;
import ch.ge.ve.interfaces.xml.codec.stream.model.Ech222Context;
import ch.ge.ve.interfaces.xml.ech.eCH0058.v5.HeaderType;
import ch.ge.ve.interfaces.xml.ech.eCH0222.v1.Delivery;
import ch.ge.ve.interfaces.xml.ech.eCH0222.v1.ReportingBodyType;
import ch.ge.ve.interfaces.xml.ech.eCH0222.v1.VoteRawDataType.BallotRawData.BallotCasted;
import ch.ge.ve.interfaces.xml.util.CachingJAXBContextFactory;
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRule;
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRulesFactory;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Stream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * {@code XmlStreamingCodec} implementation for the eCH-0222 format.
 */
final class Ech222StreamingCodec extends AbstractStreamingCodec<CastBallot, StreamedEch222> {

  private static final String BALLOT_CASTED_TAG_NAME            = "ballotCasted";
  private static final String DELIVERY_HEADER_TAG_NAME          = "deliveryHeader";
  private static final String REPORTING_BODY_TAG_NAME           = "reportingBody";
  private static final String CONTEST_IDENTIFICATION_TAG_NAME   = "contestIdentification";
  private static final String COUNTING_CIRCLE_RAW_DATA_TAG_NAME = "countingCircleRawData";
  private static final String COUNTING_CIRCLE_ID_TAG_NAME       = "countingCircleId";
  private static final String VOTE_IDENTIFICATION_TAG_NAME      = "voteIdentification";
  private static final String BALLOT_IDENTIFICATION_TAG_NAME    = "ballotIdentification";

  private final JAXBContext                    context;
  private final XMLInputFactory                xmlInputFactory;
  private final StreamedValidationRulesFactory rulesFactory;

  /**
   * Creates a new {@code Ech222StreamingCodec}.
   *
   * @param xmlInputFactory the XML input factory to use.
   * @param configuration   the codec configuration.
   * @param rulesFactory    the "business" rules factory.
   *
   * @throws NullPointerException if any of the given arguments is {@code null}.
   */
  Ech222StreamingCodec(XMLInputFactory xmlInputFactory, CodecConfiguration configuration,
                       StreamedValidationRulesFactory rulesFactory) {
    super(configuration);
    this.context = CachingJAXBContextFactory.getJaxbContextFor(Delivery.class);
    this.xmlInputFactory = Objects.requireNonNull(xmlInputFactory);
    this.rulesFactory = Objects.requireNonNull(rulesFactory);
  }

  @Override
  protected StreamedEch222 createStreamedXmlObject(XmlSource source) {
    Ech222Context ech222Context = readContext(source);
    Supplier<Stream<CastBallot>> ballots = castBallotsSupplier(source, ech222Context.getContestIdentification());
    return new StreamedEch222(ech222Context, ballots);
  }

  private Ech222Context readContext(XmlSource source) {
    return source.read(inputStream -> {
      try {
        ContextBuilder builder = new ContextBuilder();
        XmlElementConsumerRegistry registry = new XmlElementConsumerRegistry(context.createUnmarshaller());
        registry.registerConsumer(DELIVERY_HEADER_TAG_NAME, HeaderType.class, builder::setHeader);
        registry.registerConsumer(REPORTING_BODY_TAG_NAME, ReportingBodyType.class, builder::setReportingBody);
        registry.registerConsumer(CONTEST_IDENTIFICATION_TAG_NAME, String.class, builder::setContestIdentification);
        XmlStreamWalker walker = new XmlStreamWalker(registry, xmlInputFactory.createXMLStreamReader(inputStream));
        walker.walkUntil(XmlElementMatcher.startOf(COUNTING_CIRCLE_RAW_DATA_TAG_NAME));
        builder.setCountingCircleIdsSupplier(countingCircleIdsSupplier(source));
        return builder.build();
      } catch (XMLStreamException | JAXBException e) {
        throw new XmlDeserializationException("Deserialization failed", e);
      }
    });
  }

  private Supplier<Stream<String>> countingCircleIdsSupplier(XmlSource source) {
    ElementsExtractor extractor = new ElementsExtractor(xmlInputFactory, context, source);
    return () -> extractor.extract(COUNTING_CIRCLE_ID_TAG_NAME, String.class);
  }

  private Supplier<Stream<CastBallot>> castBallotsSupplier(XmlSource source, String contestIdentification) {
    ElementsExtractor extractor = new ElementsExtractor(xmlInputFactory, context, source);
    return () -> extractor.extract(
        (reader, unmarshaller) -> new CastBallotIterator(unmarshaller, reader, contestIdentification)
    );
  }

  @Override
  protected Stream<StreamedValidationRule<CastBallot>> allExistingRules(StreamedEch222 ech222) {
    return rulesFactory.createEch222ValidationRules(ech222.getContext()).stream();
  }

  private static final class CastBallotIterator implements Iterator<CastBallot> {

    private final CastBallotBuilder builder;
    private final XmlStreamWalker   walker;

    CastBallotIterator(Unmarshaller unmarshaller, XMLStreamReader reader, String contestIdentification) {
      this.builder = new CastBallotBuilder().contestIdentification(contestIdentification);
      XmlElementConsumerRegistry registry = new XmlElementConsumerRegistry(unmarshaller);
      registry.registerConsumer(COUNTING_CIRCLE_ID_TAG_NAME, String.class, builder::countingCircleId);
      registry.registerConsumer(VOTE_IDENTIFICATION_TAG_NAME, String.class, builder::voteIdentification);
      registry.registerConsumer(BALLOT_IDENTIFICATION_TAG_NAME, String.class, builder::ballotIdentification);
      registry.registerConsumer(BALLOT_CASTED_TAG_NAME, BallotCasted.class, builder::ballotCasted);
      this.walker = new XmlStreamWalker(registry, reader);
      this.walker.walkUntil(XmlElementMatcher.startOf(BALLOT_CASTED_TAG_NAME));
      readNext();
    }

    @Override
    public boolean hasNext() {
      return builder.ballotCasted() != null;
    }

    @Override
    public CastBallot next() {
      if (!hasNext()) {
        throw new NoSuchElementException("End of stream");
      }
      CastBallot current = builder.build();
      readNext();
      return current;
    }

    private void readNext() {
      builder.ballotCasted(null);
      walker.walkUntil(XmlElementMatcher.startOf(BALLOT_CASTED_TAG_NAME));
    }
  }

  private static final class ContextBuilder {

    private HeaderType               header;
    private ReportingBodyType        reportingBody;
    private String                   contestIdentification;
    private Supplier<Stream<String>> countingCircleIdsSupplier;

    void setHeader(HeaderType header) {
      this.header = header;
    }

    void setReportingBody(ReportingBodyType reportingBody) {
      this.reportingBody = reportingBody;
    }

    void setContestIdentification(String contestIdentification) {
      this.contestIdentification = contestIdentification;
    }

    void setCountingCircleIdsSupplier(Supplier<Stream<String>> countingCircleIdsSupplier) {
      this.countingCircleIdsSupplier = countingCircleIdsSupplier;
    }

    Ech222Context build() {
      return new Ech222Context(header, reportingBody, contestIdentification, countingCircleIdsSupplier);
    }
  }
}
