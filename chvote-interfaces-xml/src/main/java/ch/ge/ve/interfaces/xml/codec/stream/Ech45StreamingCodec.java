/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream;

import ch.ge.ve.interfaces.xml.XmlSource;
import ch.ge.ve.interfaces.xml.codec.CodecConfiguration;
import ch.ge.ve.interfaces.xml.codec.XmlDeserializationException;
import ch.ge.ve.interfaces.xml.codec.stream.model.Ech45Context;
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.AuthorityType;
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.ContestType;
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.VoterDelivery;
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.VotingPersonType;
import ch.ge.ve.interfaces.xml.ech.eCH0058.v5.HeaderType;
import ch.ge.ve.interfaces.xml.util.CachingJAXBContextFactory;
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRule;
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRulesFactory;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Stream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;

/**
 * {@code XmlStreamingCodec} implementation for the eCH-0045 format.
 */
final class Ech45StreamingCodec extends AbstractStreamingCodec<VotingPersonType, StreamedEch45> {

  private static final String VOTER_TAG_NAME               = "voter";
  private static final String DELIVERY_HEADER_TAG_NAME     = "deliveryHeader";
  private static final String CONTEST_TAG_NAME             = "contest";
  private static final String REPORTING_AUTHORITY_TAG_NAME = "reportingAuthority";
  private static final String NUMBER_OF_VOTERS_TAG_NAME    = "numberOfVoters";

  private final JAXBContext                    context;
  private final XMLInputFactory                xmlInputFactory;
  private final StreamedValidationRulesFactory rulesFactory;

  /**
   * Creates a new {@code Ech45StreamingCodec}.
   *
   * @param xmlInputFactory the XML input factory to use.
   * @param configuration   the codec configuration.
   * @param rulesFactory    the "business" rules factory.
   *
   * @throws NullPointerException if any of the given arguments is {@code null}.
   */
  Ech45StreamingCodec(XMLInputFactory xmlInputFactory, CodecConfiguration configuration,
                      StreamedValidationRulesFactory rulesFactory) {
    super(configuration);
    this.context = CachingJAXBContextFactory.getJaxbContextFor(VoterDelivery.class);
    this.xmlInputFactory = Objects.requireNonNull(xmlInputFactory);
    this.rulesFactory = Objects.requireNonNull(rulesFactory);
  }

  @Override
  protected StreamedEch45 createStreamedXmlObject(XmlSource source) {
    return new StreamedEch45(readContext(source), votersSupplier(source));
  }

  private Ech45Context readContext(XmlSource source) {
    return source.read(inputStream -> {
      try {
        ContextBuilder builder = new ContextBuilder();
        XmlElementConsumerRegistry registry = new XmlElementConsumerRegistry(context.createUnmarshaller());
        registry.registerConsumer(DELIVERY_HEADER_TAG_NAME, HeaderType.class, builder::setHeader);
        registry.registerConsumer(CONTEST_TAG_NAME, ContestType.class, builder::setContest);
        registry.registerConsumer(REPORTING_AUTHORITY_TAG_NAME, AuthorityType.class, builder::setReportingAuthority);
        registry.registerConsumer(NUMBER_OF_VOTERS_TAG_NAME, Long.class, builder::setNumberOfVoters);
        XmlStreamWalker walker = new XmlStreamWalker(registry, xmlInputFactory.createXMLStreamReader(inputStream));
        walker.walkUntil(XmlElementMatcher.startOf(VOTER_TAG_NAME));
        return builder.build();
      } catch (XMLStreamException | JAXBException e) {
        throw new XmlDeserializationException("Deserialization failed", e);
      }
    });
  }

  private Supplier<Stream<VotingPersonType>> votersSupplier(XmlSource source) {
    ElementsExtractor extractor = new ElementsExtractor(xmlInputFactory, context, source);
    return () -> extractor.extract(VOTER_TAG_NAME, VotingPersonType.class);
  }

  @Override
  protected Stream<StreamedValidationRule<VotingPersonType>> allExistingRules(StreamedEch45 ech45) {
    return rulesFactory.createEch45ValidationRules(ech45.getContext()).stream();
  }

  private static final class ContextBuilder {

    private HeaderType    header;
    private AuthorityType reportingAuthority;
    private Long          numberOfVoters;
    private ContestType   contest;

    void setHeader(HeaderType header) {
      this.header = header;
    }

    void setReportingAuthority(AuthorityType reportingAuthority) {
      this.reportingAuthority = reportingAuthority;
    }

    void setNumberOfVoters(Long numberOfVoters) {
      this.numberOfVoters = numberOfVoters;
    }

    void setContest(ContestType contest) {
      this.contest = contest;
    }

    Ech45Context build() {
      return new Ech45Context(header, reportingAuthority, numberOfVoters, contest);
    }
  }
}
