/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.validation;

import ch.ge.ve.interfaces.xml.XmlSource;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 * Static access to the global ChVote schema definition.
 */
public final class ChVoteSchema {

  private static final Logger logger = LoggerFactory.getLogger(ChVoteSchema.class);
  private static final Schema schema = new ChVoteSchema().createSchema();

  // Instance of ChVoteSchema is used as an internal builder to create the Schema object
  private ChVoteSchema() { /* For internal use only */ }

  /**
   * Returns the ChVote {@link Schema} object regrouping all ChVote XSDs in a global schema definition.
   *
   * @return the global ChVote schema.
   */
  public static Schema get() {
    return schema;
  }

  /**
   * Convenient method to validate a ChVote XML source against the XSD schemas. If you need finer control over the
   * validation, you can access the {@link Schema} object using {@link #get()}.
   *
   * @param source the XML source.
   *
   * @throws IllegalArgumentException  if the specified file cannot be read.
   * @throws SchemaValidationException if the specified file does not conform to the schema.
   */
  public static void validate(XmlSource source) {
    source.read((Consumer<InputStream>) ChVoteSchema::validate);
  }

  /**
   * Convenient method to validate a ChVote XML stream against the XSD schemas. If you need finer control over the
   * validation, you can access the {@link Schema} object using {@link #get()}.
   *
   * @param inputStream the source input stream.
   *
   * @throws IllegalArgumentException  if the given stream cannot be read.
   * @throws SchemaValidationException if the given stream does not conform to the schema.
   */
  public static void validate(InputStream inputStream) {
    try {
      Validator validator = schema.newValidator();
      // protection against XXE
      // see https://www.owasp.org/index.php/XML_External_Entity_(XXE)_Prevention_Cheat_Sheet for more information
      validator.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
      validator.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
      validator.validate(nonClosingSourceFrom(inputStream));
    } catch (IOException e) {
      throw new UncheckedIOException("Cannot read input stream", e);
    } catch (SAXException saxe) {
      throw new SchemaValidationException("Source is not XSD-valid", saxe);
    }
  }

  // Validator.validate(StreamSource) closes the source, even if it didn't open the stream in the first place
  //   -> this workaround prevent it to do so by swallowing the call to "InputStream.close()"
  private static Source nonClosingSourceFrom(InputStream inputStream) {
    return new StreamSource(new FilterInputStream(inputStream) {
      @Override
      public void close() {
        // the responsibility for the call to close() is not ours
      }
    });
  }

  private Schema createSchema() {
    try (Stream<StreamSource> sources = getSchemaSources()) {
      SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
      // protection against XXE
      // see https://www.owasp.org/index.php/XML_External_Entity_(XXE)_Prevention_Cheat_Sheet for more information
      sf.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
      sf.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
      return sf.newSchema(sources.toArray(Source[]::new));
    } catch (SAXException e) {
      throw new IllegalStateException("Can't read schemas...", e);
    }
  }

  private Stream<StreamSource> getSchemaSources() {
    final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    List<InputStream> inputStreams = new ArrayList<>();
    // Caution: order matters here!
    // The dependency tree be must traversed depth first (imported schemas must be listed before).
    return Stream.of("xsd/eCH-0044-4-1.xsd", "xsd/eCH-0010-5-1.xsd", "xsd/eCH-0010-6-0.xsd", "xsd/eCH-0006-2-0.xsd",
                     "xsd/eCH-0008-3-0.xsd", "xsd/eCH-0007-5-0.xsd", "xsd/eCH-0007-6-0.xsd", "xsd/eCH-0135-1-0.xsd",
                     "xsd/eCH-0058-5-0.xsd", "xsd/eCH-0011-8-1.xsd", "xsd/eCH-0021-7-0.xsd", "xsd/eCH-0155-3-0.xsd",
                     "xsd/eCH-0155-4-0.xsd", "xsd/eCH-0222-1-0.xsd", "xsd/eCH-0110-4-0.xsd", "xsd/eCH-0157-4-0.xsd",
                     "xsd/eCH-0159-4-0.xsd", "xsd/eCH-0045-4-0.xsd", "xsd/eCH-0228-1-0.xsd", "xsd/eCH-0071-1-1.xsd",
                     "xsd/logistic-delivery.xsd", "xsd/counting-circle-1-0.xsd")
                 .peek(path -> logger.debug("Loading XSD definition from classpath : {}", path))
                 .map(classLoader::getResourceAsStream)
                 .peek(inputStreams::add)    // register InputStream for onClose
                 .map(StreamSource::new)
                 .onClose(() -> closeAll(inputStreams));
  }

  // Ensures all InputStream objects are closed, even if the call to close() throws an exception on a particular item
  private void closeAll(List<InputStream> streams) {
    logger.debug("{} InputStream objects must be closed", streams.size());
    final List<Exception> exceptions = new ArrayList<>(streams.size());
    for (InputStream inputStream : streams) {
      try {
        inputStream.close();
      } catch (Exception e) {
        logger.error("Failed to close an InputStream", e);
        exceptions.add(e);
      }
    }
    if (!exceptions.isEmpty()) {
      final IllegalStateException exception = new IllegalStateException(
          "Failed to close all InputStreams used to load the JAXB Schema - see suppressed exceptions and logs");
      exceptions.forEach(exception::addSuppressed);
      throw exception;
    }
  }
}
