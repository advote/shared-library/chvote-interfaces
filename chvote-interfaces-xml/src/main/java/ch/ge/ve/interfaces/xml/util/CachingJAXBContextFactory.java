/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.util;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

/**
 * Caching JAXB context factory.
 */
public final class CachingJAXBContextFactory {

  private static final ConcurrentHashMap<Class<?>, JAXBContext> cache = new ConcurrentHashMap<>();

  /**
   * Returns the JAXB context for the specified class. If a context has already been requested for the given class,
   * then the context is read from the cache, otherwise a new instance is created and cached for future requests.
   *
   * @param c the class that needs to be recognized by the desired context.
   *
   * @return the corresponding JAXB context.
   *
   * @throws NullPointerException if {@code c} is {@code null}.
   */
  public static JAXBContext getJaxbContextFor(Class<?> c) {
    Objects.requireNonNull(c);
    return cache.computeIfAbsent(c, cls -> {
      try {
        return JAXBContext.newInstance(cls);
      } catch (JAXBException e) {
        throw new IllegalStateException("Initialization failure...", e);
      }
    });
  }

  private CachingJAXBContextFactory() {
    throw new AssertionError("Not meant to be instantiated");
  }
}
