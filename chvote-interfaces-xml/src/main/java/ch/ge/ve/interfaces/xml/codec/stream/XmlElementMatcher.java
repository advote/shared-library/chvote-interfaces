/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream;

import java.util.Objects;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;

/**
 * Verifies whether an {@code XMLStreamReader} is positioned on a particular element.
 */
final class XmlElementMatcher {

  private final String name;
  private final int    type;

  public static XmlElementMatcher startOf(String localName) {
    return new XmlElementMatcher(Objects.requireNonNull(localName), XMLStreamConstants.START_ELEMENT);
  }

  private XmlElementMatcher(String name, int type) {
    this.name = name;
    this.type = type;
  }

  public boolean matchesCurrentPosition(XMLStreamReader reader) {
    return type == reader.getEventType() && name.equals(reader.getLocalName());
  }
}
