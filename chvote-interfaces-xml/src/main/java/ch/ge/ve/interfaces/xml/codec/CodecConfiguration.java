/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;

/**
 * Codec configuration object. Instances of this class are immutable.
 */
public final class CodecConfiguration {

  private final boolean     escapeUsingCDataSection;
  private final boolean     formattedOutput;
  private final boolean     validation;
  private final Set<String> disabledValidationRules;

  /**
   * Creates a new {@code CodecConfiguration} instance with default parameter values (escapeUsingCDataSection=false,
   * formattedOutput=false, validation=true and disabledValidationRules=[]).
   */
  public CodecConfiguration() {
    this(false, false, true, Collections.emptySet());
  }

  private CodecConfiguration(boolean escapeUsingCDataSection, boolean formattedOutput,
                             boolean validation, Set<String> disabledValidationRules) {
    this.escapeUsingCDataSection = escapeUsingCDataSection;
    this.formattedOutput = formattedOutput;
    this.validation = validation;
    this.disabledValidationRules = disabledValidationRules;
  }

  /**
   * Returns a new {@code CodecConfiguration} instance with CDATA escaping enabled.
   * <p>
   * Text elements are put in a CDATA section when they contain at least one of
   * the following characters : {@code "&, <, >"}.
   * </p>
   *
   * @return a new {@code CodecConfiguration} instance with CDATA escaping enabled.
   */
  public CodecConfiguration withEscapeUsingCDataSectionEnabled() {
    return new CodecConfiguration(true, formattedOutput, validation, disabledValidationRules);
  }

  /**
   * Returns a new {@code CodecConfiguration} instance with CDATA escaping disabled.
   *
   * @return a new {@code CodecConfiguration} instance with CDATA escaping disabled.
   */
  public CodecConfiguration withEscapeUsingCDataSectionDisabled() {
    return new CodecConfiguration(false, formattedOutput, validation, disabledValidationRules);
  }

  /**
   * Returns a new {@code CodecConfiguration} instance with formatted enabled.
   *
   * @return a new {@code CodecConfiguration} instance with formatted enabled.
   */
  public CodecConfiguration withFormattedOutputEnabled() {
    return new CodecConfiguration(escapeUsingCDataSection, true, validation, disabledValidationRules);
  }

  /**
   * Returns a new {@code CodecConfiguration} instance with formatted output disabled.
   *
   * @return a new {@code CodecConfiguration} instance with formatted output disabled.
   */
  public CodecConfiguration withFormattedOutputDisabled() {
    return new CodecConfiguration(escapeUsingCDataSection, false, validation, disabledValidationRules);
  }

  /**
   * Returns a new {@code CodecConfiguration} instance with validation enabled.
   *
   * @return a new {@code CodecConfiguration} instance with the validation enabled.
   */
  public CodecConfiguration withValidationEnabled() {
    return new CodecConfiguration(escapeUsingCDataSection, formattedOutput, true, disabledValidationRules);
  }

  /**
   * Returns a new {@code CodecConfiguration} instance with validation disabled.
   *
   * @return a new {@code CodecConfiguration} instance with validation disabled.
   */
  public CodecConfiguration withValidationDisabled() {
    return new CodecConfiguration(escapeUsingCDataSection, formattedOutput, false, disabledValidationRules);
  }

  /**
   * Returns a new {@code CodecConfiguration} instance with the specified validation rules disabled.
   *
   * @param rules the disabled rules.
   *
   * @return a new {@code CodecConfiguration} instance with the specified validation rules disabled.
   *
   * @throws NullPointerException if {@code rules} is {@code null} or if it contains a {@code null} reference.
   */
  public CodecConfiguration withDisabledValidationRules(String... rules) {
    return withDisabledValidationRules(Arrays.asList(rules));
  }

  /**
   * Returns a new {@code CodecConfiguration} instance with the specified validation rules disabled.
   *
   * @param rules the disabled rules.
   *
   * @return a new {@code CodecConfiguration} instance with the specified validation rules disabled.
   *
   * @throws NullPointerException if {@code rules} is {@code null} or if it contains a {@code null} reference.
   */
  public CodecConfiguration withDisabledValidationRules(Collection<String> rules) {
    Set<String> disabledRules = Collections.unmodifiableSet(new LinkedHashSet<>(rules));
    disabledRules.forEach(Objects::requireNonNull);
    return new CodecConfiguration(escapeUsingCDataSection, formattedOutput, validation, disabledRules);
  }

  /**
   * Returns whether the text elements should be put in a CDATA section when they contain at least one of
   * the following characters : {@code "&, <, >"}.
   *
   * @return the CDATA option configuration.
   */
  public boolean isEscapeUsingCDataSectionEnabled() {
    return escapeUsingCDataSection;
  }

  /**
   * Returns whether the output must be formatted.
   *
   * @return whether the output must be formatted.
   */
  public boolean isFormattedOutputEnabled() {
    return formattedOutput;
  }

  /**
   * Returns whether validation is enabled.
   *
   * @return whether validation is enabled.
   */
  public boolean isValidationEnabled() {
    return validation;
  }

  /**
   * Returns the disabled validation rules.
   *
   * @return the disabled validation rules.
   */
  public Set<String> getDisabledValidationRules() {
    return disabledValidationRules;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CodecConfiguration that = (CodecConfiguration) o;
    return escapeUsingCDataSection == that.escapeUsingCDataSection && formattedOutput == that.formattedOutput
           && validation == that.validation && disabledValidationRules.equals(that.disabledValidationRules);
  }

  @Override
  public int hashCode() {
    return Objects.hash(escapeUsingCDataSection, formattedOutput, validation, disabledValidationRules);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", CodecConfiguration.class.getSimpleName() + "[", "]")
        .add("escapeUsingCDataSection=" + escapeUsingCDataSection)
        .add("formattedOutput=" + formattedOutput)
        .add("validation=" + validation)
        .add("disabledValidationRules=" + disabledValidationRules)
        .toString();
  }
}
