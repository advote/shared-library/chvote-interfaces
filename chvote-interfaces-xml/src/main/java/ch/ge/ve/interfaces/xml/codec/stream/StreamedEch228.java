/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream;

import ch.ge.ve.interfaces.xml.codec.stream.model.Ech228Context;
import ch.ge.ve.interfaces.xml.ech.eCH0228.v1.VotingCardDeliveryType.VotingCard;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * {@code StreamedXml} implementation for the eCH-0228 format.
 */
public final class StreamedEch228 extends StreamedXml<VotingCard> {

  private final Ech228Context context;

  /**
   * Creates a new {@code StreamedEch228}.
   *
   * @param context     the context of the streamed element.
   * @param votingCards the voting cards stream supplier.
   *
   * @throws NullPointerException if any of the given arguments is {@code null}.
   */
  StreamedEch228(Ech228Context context, Supplier<Stream<VotingCard>> votingCards) {
    super(votingCards);
    this.context = Objects.requireNonNull(context);
  }

  /**
   * Returns the stream's context.
   *
   * @return the stream's context.
   */
  public Ech228Context getContext() {
    return context;
  }
}
