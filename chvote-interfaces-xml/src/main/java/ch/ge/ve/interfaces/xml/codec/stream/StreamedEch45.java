/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream;

import ch.ge.ve.interfaces.xml.codec.stream.model.Ech45Context;
import ch.ge.ve.interfaces.xml.ech.eCH0045.v4.VotingPersonType;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * {@code StreamedXml} implementation for the eCH-0045 format.
 */
public final class StreamedEch45 extends StreamedXml<VotingPersonType> {

  private final Ech45Context context;

  /**
   * Creates a new {@code StreamedEch45}.
   *
   * @param context the context of the streamed element.
   * @param voters  the voters stream supplier.
   *
   * @throws NullPointerException if any of the given arguments is {@code null}.
   */
  StreamedEch45(Ech45Context context, Supplier<Stream<VotingPersonType>> voters) {
    super(voters);
    this.context = Objects.requireNonNull(context);
  }

  /**
   * Returns the stream's context.
   *
   * @return the stream's context.
   */
  public Ech45Context getContext() {
    return context;
  }
}
