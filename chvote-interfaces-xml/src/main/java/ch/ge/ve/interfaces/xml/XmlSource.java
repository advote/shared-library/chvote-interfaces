/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * A source of {@code InputStream}s. Supplies a new {@link InputStream} each time its {@link #open()} method is invoked.
 */
public interface XmlSource {

  /**
   * Creates a new {@code XmlSource} from the given file.
   *
   * @param sourcePath the source file's path.
   *
   * @return the created the {@code XmlSource}.
   *
   * @throws NullPointerException if {@code sourcePath} is {@code null}.
   */
  static XmlSource from(Path sourcePath) {
    Objects.requireNonNull(sourcePath);
    return () -> {
      try {
        return Files.newInputStream(sourcePath);
      } catch (IOException e) {
        throw new UncheckedIOException(e);
      }
    };
  }

  /**
   * Creates a new {@code XmlSource} from the given bytes.
   *
   * @param bytes the source's content.
   *
   * @return the created the {@code XmlSource}.
   *
   * @throws NullPointerException if {@code bytes} is {@code null}.
   */
  static XmlSource from(byte[] bytes) {
    Objects.requireNonNull(bytes);
    return () -> new ByteArrayInputStream(bytes);
  }

  /**
   * Opens the source and passes the obtained {@code InputStream} to the given consumer. The stream is then
   * automatically closed when the consumer is done.
   *
   * @param processor the stream processor.
   *
   * @throws UncheckedIOException if an I/O error occurs during the process.
   */
  default void read(Consumer<InputStream> processor) {
    try (InputStream in = open()) {
      processor.accept(in);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  /**
   * Opens the source and passes the obtained {@code InputStream} to the given processor. The stream is then
   * automatically closed when the processor is done and the result of the process is returned.
   *
   * @param processor the stream processor.
   *
   * @throws UncheckedIOException if an I/O error occurs during the process.
   */
  default <T> T read(Function<InputStream, T> processor) {
    try (InputStream in = open()) {
      return processor.apply(in);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  /**
   * Returns a new {@code InputStream} to read from.
   *
   * @return a new {@code InputStream}.
   *
   * @throws UncheckedIOException if an I/O errors occurs during the process.
   */
  InputStream open();
}
