/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.util;

import javax.xml.stream.XMLInputFactory;

public final class CachingXMLInputFactory {

  private static final XMLInputFactory instance;
  static {
    instance = XMLInputFactory.newInstance();
    // disable DTDs and external entities
    // see https://www.owasp.org/index.php/XML_External_Entity_(XXE)_Prevention_Cheat_Sheet for more information
    instance.setProperty(XMLInputFactory.SUPPORT_DTD, false);
    instance.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
    instance.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, false);
  }

  public static XMLInputFactory getInstance() {
    return instance;
  }

  private CachingXMLInputFactory() {
    throw new AssertionError("Not meant to be instantiated");
  }
}
