/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream;

import ch.ge.ve.interfaces.xml.XmlSource;
import ch.ge.ve.interfaces.xml.codec.CodecConfiguration;
import ch.ge.ve.interfaces.xml.codec.XmlDeserializationException;
import ch.ge.ve.interfaces.xml.validation.BusinessValidationException;
import ch.ge.ve.interfaces.xml.validation.ChVoteSchema;
import ch.ge.ve.interfaces.xml.validation.SchemaValidationException;
import ch.ge.ve.interfaces.xml.validation.Violation;
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRule;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Template implementation of {@link XmlStreamingCodec} that abstracts the validation layer (XSD + business rules).
 *
 * @param <E> the type of elements that are streamed
 * @param <S> the streaming container concrete type
 */
abstract class AbstractStreamingCodec<E, S extends StreamedXml<E>> implements XmlStreamingCodec<S> {

  private final CodecConfiguration configuration;

  protected AbstractStreamingCodec(CodecConfiguration configuration) {
    this.configuration = Objects.requireNonNull(configuration);
  }

  /**
   * Reads the given XML stream and deserializes it into the corresponding Java object.
   * <p>
   *   If the configuration of this instance of the codec enables validation, the XML stream is also
   *   XSD-validated (in one pass prior to any deserialization attempt, using {@link ChVoteSchema})
   *   and confronted to the business rules defined for that type of object (see {@link StreamedValidationRule}).
   *   Note that business rules can be disabled by name in the configuration object.
   * </p>
   *
   * @param source the source to read from.
   *
   * @return the deserialized object.
   *
   * @throws XmlDeserializationException if an error occurred on JAXB deserialization
   * @throws SchemaValidationException if validation is enabled and the XSD has been violated
   * @throws BusinessValidationException if validation is enabled and at least one (active) business rule has been violated
   */
  @Override
  public S deserialize(XmlSource source) {
    // [Validation] Pre-check : the source must be XSD-valid before we even try to create a streaming object
    if (configuration.isValidationEnabled()) {
      ChVoteSchema.validate(source);
    }
    S streamedXmlObject = createStreamedXmlObject(source);
    // [Validation] ..then the object is tested against the business rules before being returned to the caller
    if (configuration.isValidationEnabled()) {
      checkBusinessRules(streamedXmlObject);
    }
    return streamedXmlObject;
  }

  /**
   * Creates the {@code StreamedXml} instance that will allow to stream-read the source document.
   * <p>
   *   This method is called each time {@link #deserialize(XmlSource) deserialize} is executed, in order to
   *   instantiate the exposed object. In other words, this is the instance that will be validation-tested
   *   then returned to the caller in the end.
   * </p>
   *
   * @param source the source document
   * @return a new instance of the concrete StreamedXml type exposed by this StreamingCodec implementation
   */
  protected abstract S createStreamedXmlObject(XmlSource source);

  private void checkBusinessRules(S streamedXmlObject) {
    List<StreamedValidationRule<E>> rules = getBusinessValidationRules(streamedXmlObject);

    if (!rules.isEmpty()) {
      try (Stream<E> elementsStream = streamedXmlObject.stream()) {
        elementsStream.forEach(element -> rules.forEach(rule -> rule.onElement(element)));
      }
      List<Violation> violations = rules.stream().flatMap(rule -> rule.validate().stream()).collect(Collectors.toList());
      if (!violations.isEmpty()) {
        throw new BusinessValidationException(violations);
      }
    }
  }

  private List<StreamedValidationRule<E>> getBusinessValidationRules(S streamedXmlObject) {
    return allExistingRules(streamedXmlObject)
        .filter(rule -> !configuration.getDisabledValidationRules().contains(rule.getName()))
        .collect(Collectors.toList());
  }

  /**
   * Creates a stream of all rules applicable to the given {@code StreamedXml} object.
   * <p>
   *   This method is called each time the business rules have to be applied to a StreamedXml object.
   *   The implementation does not need to filter out disabled rules : this will be done internally by
   *   AbstractStreamingCodec.
   * </p>
   *
   * @param streamedXmlObject the object to be tested
   * @return all defined rules for such an object, instantiated in the context (if any) of the given object
   */
  protected abstract Stream<StreamedValidationRule<E>> allExistingRules(S streamedXmlObject);
}
