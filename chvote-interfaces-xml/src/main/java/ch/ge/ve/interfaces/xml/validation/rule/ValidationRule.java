/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.validation.rule;

import ch.ge.ve.interfaces.xml.validation.Violation;
import java.util.List;

/**
 * Base representation of a validation rule. Note that validation rules are only invoked if validation is enabled and
 * the read/generated XML is XSD-valid. Implementations are not required to be thread safe.
 *
 * @param <T> the type of elements validated by the rule.
 */
public interface ValidationRule<T> {

  /**
   * Returns the rule's name (defaults to the class name).
   *
   * @return the rule's name.
   */
  default String getName() {
    return getClass().getName();
  }

  /**
   * Validates the given object and returns all found violations.
   *
   * @param element the object to validate.
   *
   * @return all found violations.
   */
  List<Violation> validate(T element);
}
