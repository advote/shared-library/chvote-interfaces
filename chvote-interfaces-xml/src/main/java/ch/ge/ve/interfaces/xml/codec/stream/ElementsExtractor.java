/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream;

import ch.ge.ve.interfaces.xml.XmlSource;
import ch.ge.ve.interfaces.xml.codec.XmlDeserializationException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Spliterators;
import java.util.function.BiFunction;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * Utility class to easily extract and unmarshal all occurrences of a particular XML element.
 */
final class ElementsExtractor {

  private final XmlSource       source;
  private final JAXBContext     context;
  private final XMLInputFactory xmlInputFactory;

  ElementsExtractor(XMLInputFactory xmlInputFactory, JAXBContext context, XmlSource source) {
    this.xmlInputFactory = Objects.requireNonNull(xmlInputFactory);
    this.source = Objects.requireNonNull(source);
    this.context = Objects.requireNonNull(context);
  }

  /**
   * Opens an XML Stream from the configured source and prepare a Java {@code Stream} out of it.
   * <p>
   *   The stream will iterate through all XML elements of the document, defined by the XML tag name and the
   *   Java class they are bound to (by JAXB).
   * </p>
   *
   * @param elementName the name of the XML tag that identifies the elements to collect (namespace-agnostic)
   * @param elementType the Java class targeted by the element's unmarshalling
   * @param <E> the type of elements that will be streamed
   *
   * @return a stream of the defined element and configured to close the InputStream on {@code Stream.close()}
   */
  public <E> Stream<E> extract(String elementName, Class<E> elementType) {
    return extract((reader, unmarshaller) -> new ElementsIterator<>(unmarshaller, reader, elementName, elementType));
  }

  /**
   * Opens an XML Stream from the configured source and prepare a Java {@code Stream} out of it.
   * <p>
   *   The elements collected by the stream are defined via an {@code Iterator}. This method lets the caller
   *   define the iterator implementation to use.
   * </p>
   *
   * @param iteratorSupplier how to create the iterator from the opened XML stream and using the contextual unmarshaller
   * @param <E> the type of elements that will be streamed
   *
   * @return a stream based on the created iterator and configured to close the InputStream on {@code Stream.close()}
   *
   * @see #extract(String, Class)
   */
  <E> Stream<E> extract(BiFunction<XMLStreamReader, Unmarshaller, Iterator<E>> iteratorSupplier) {
    try {
      final InputStream inputStream = source.open();
      XMLStreamReader streamReader = xmlInputFactory.createXMLStreamReader(inputStream);
      Unmarshaller unmarshaller = context.createUnmarshaller();
      Iterator<E> iterator = iteratorSupplier.apply(streamReader, unmarshaller);
      return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, 0), false)
                          .onClose(() -> Io.close(inputStream));
    } catch (XMLStreamException | JAXBException e) {
      throw new XmlDeserializationException("Deserialization failed", e);
    }
  }

  // An iterator that walks through an XML stream, stopping on every element of one kind and unmarshalling it.
  private static final class ElementsIterator<E> implements Iterator<E> {

    private final String          elementName;
    private final XmlStreamWalker walker;
    private       E               nextElement;

    ElementsIterator(Unmarshaller unmarshaller, XMLStreamReader reader, String elementName, Class<E> elementType) {
      this.elementName = elementName;
      XmlElementConsumerRegistry registry = new XmlElementConsumerRegistry(unmarshaller);
      registry.registerConsumer(elementName, elementType, e -> nextElement = e);
      this.walker = new XmlStreamWalker(registry, reader);
      this.walker.walkUntil(XmlElementMatcher.startOf(elementName));
      readNext();
    }

    @Override
    public boolean hasNext() {
      return nextElement != null;
    }

    @Override
    public E next() {
      if (!hasNext()) {
        throw new NoSuchElementException("End of stream");
      }
      E current = nextElement;
      readNext();
      return current;
    }

    private void readNext() {
      nextElement = null;
      walker.walkUntil(XmlElementMatcher.startOf(elementName));
    }
  }
}
