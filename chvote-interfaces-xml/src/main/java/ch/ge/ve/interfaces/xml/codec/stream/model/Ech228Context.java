/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream.model;

import ch.ge.ve.interfaces.xml.ech.eCH0058.v5.HeaderType;
import ch.ge.ve.interfaces.xml.ech.eCH0228.v1.NamedCodeType;
import ch.ge.ve.interfaces.xml.ech.eCH0228.v1.VotingCardDeliveryType.ContestData;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;


/**
 * Represents the "context" (the fixed part) of an eCH-0228 file.
 */
public final class Ech228Context {

  private final HeaderType                      header;
  private final ContestData                     contestData;
  private final Supplier<Stream<NamedCodeType>> logisticCodesSupplier;

  public Ech228Context(HeaderType header, ContestData contestData,
                       Supplier<Stream<NamedCodeType>> logisticCodesSupplier) {
    this.header = header;
    this.contestData = contestData;
    this.logisticCodesSupplier = logisticCodesSupplier;
  }

  public HeaderType getHeader() {
    return header;
  }

  public Optional<ContestData> getContestData() {
    return Optional.ofNullable(contestData);
  }

  public Stream<NamedCodeType> getLogisticCodes() {
    return logisticCodesSupplier.get();
  }
}
