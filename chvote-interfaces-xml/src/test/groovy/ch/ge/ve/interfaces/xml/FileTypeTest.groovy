/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml

import spock.lang.Specification

class FileTypeTest extends Specification {

  def "it must find the correct type"() {
    expect:
    FileType.of(asXmlSource(path)) == type

    where:
    path                                  | type
    "/sample-eCH-0045-v4.xml"             | FileType.ECH_0045
    "/sample-eCH-0071-v1.xml"             | FileType.ECH_0071
    "/sample-eCH-0110-v4.xml"             | FileType.ECH_0110
    "/sample-eCH-0157-v4.xml"             | FileType.ECH_0157
    "/sample-eCH-0159-v4.xml"             | FileType.ECH_0159
    "/sample-eCH-0222-v1.xml"             | FileType.ECH_0222
    "/sample-eCH-0228-v1.xml"             | FileType.ECH_0228
    "/sample-logistic-v1.xml"             | FileType.LOGISTIC
    "/sample-counting-circle-list-v1.xml" | FileType.COUNTING_CIRCLE_LIST
  }

  def "it must throw an IllegalArgumentException on unknown XML stream"() {
    when:
    FileType.of(asXmlSource("/logback-test.xml"))

    then:
    thrown(IllegalArgumentException)
  }

  def "it must throw an IllegalArgumentException on invalid stream"() {
    when:
    FileType.of(XmlSource.from("Hello".bytes))

    then:
    thrown(IllegalArgumentException)
  }

  def "it must refuse null inputs"() {
    when:
    code.call()

    then:
    thrown(NullPointerException)

    where:
    code << [
        { _ -> FileType.of((InputStream) null) },
        { _ -> FileType.of((XmlSource) null) }
    ]
  }

  private static XmlSource asXmlSource(String path) {
    return { getClass().getResourceAsStream(path) }
  }
}
