/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec

import ch.ge.ve.interfaces.xml.ech.eCH0071.v1.Nomenclature
import java.nio.charset.Charset
import org.junit.Assert
import org.xmlunit.builder.Input
import spock.lang.Specification

class Ech71SerializationTest extends Specification {

  private static final String SAMPLE_FILE_PATH = "/sample-eCH-0071-v1.xml"

  def codec = new XmlCodecFactory().ech71Codec()

  def "a valid eCH-0071 file should be successfully parsed"() {
    given: 'a valid eCH-0071 XML file'
    InputStream stream = getClass().getResourceAsStream(SAMPLE_FILE_PATH)

    when:
    Nomenclature nomenclature = codec.deserialize(stream)

    then: 'the file should be correctly parsed'
    nomenclature != null
    nomenclature.cantons.canton.size() == 26
    nomenclature.districts.district.size() == 308
    nomenclature.municipalities.municipality.size() == 5784
  }

  def "a valid delivery should be successfully serialized to xml"() {
    given: 'a nomenclature generated from a valid eCH-0071 file'
    Nomenclature nomenclature = codec.deserialize(getClass().getResourceAsStream(SAMPLE_FILE_PATH))

    when: 'serializing the nomenclature read from the eCH-0071 file'
    OutputStream out = new ByteArrayOutputStream()
    codec.serialize(nomenclature, out)

    then: 'serialization output should be similar to the original file'
    Assert.assertThat(Input.from(new String(out.toByteArray(), Charset.forName("UTF-8"))),
                      XmlMatchers.isSimilarTo(Input.fromStream(getClass().getResourceAsStream(SAMPLE_FILE_PATH))))
  }
}
