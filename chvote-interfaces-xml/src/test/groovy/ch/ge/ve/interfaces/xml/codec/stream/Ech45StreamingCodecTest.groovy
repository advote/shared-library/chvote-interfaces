/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream

import ch.ge.ve.interfaces.xml.XmlSource
import ch.ge.ve.interfaces.xml.codec.CodecConfiguration
import ch.ge.ve.interfaces.xml.validation.BusinessValidationException
import ch.ge.ve.interfaces.xml.validation.Violation
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRule
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRulesFactory
import java.time.LocalDate
import java.util.stream.Collectors
import spock.lang.Specification

class Ech45StreamingCodecTest extends Specification {

  private static final String SAMPLE_FILE_PATH = "/sample-eCH-0045-v4.xml"

  def "read sample ech45 file"() {
    given:
    def codec = new XmlStreamingCodecFactory().ech45StreamingCodec()

    when:
    def streamedEch45 = codec.deserialize(sampleFile())

    then:
    streamedEch45.context.header.messageId == "4e94481b308c415ea5d79317508bb660"
    streamedEch45.context.reportingAuthority.cantonalRegister.registerIdentification == "8268"
    streamedEch45.context.contest.get().contestDate == LocalDate.of(2018, 8, 20)
    streamedEch45.context.contest.get().contestDescription.contestDescriptionInfo[0].contestDescription == "Test contest"

    streamedEch45.stream().count() == streamedEch45.context.numberOfVoters
    streamedEch45.stream().count() == 92

    def voters = streamedEch45.stream().collect(Collectors.toList())
    voters.first().person.swiss.swissDomesticPerson.personIdentification.localPersonId.personId == "2094670708"
    voters.last().person.swiss.swissDomesticPerson.personIdentification.localPersonId.personId == "2549336801"
  }

  def "it should apply business validation rules during deserialization when validation is enabled"() {
    given:
    def rules = [Mock(StreamedValidationRule), Mock(StreamedValidationRule)]
    def rulesFactory = Mock(StreamedValidationRulesFactory)
    def codec = new XmlStreamingCodecFactory(rulesFactory).ech45StreamingCodec()

    when:
    codec.deserialize(sampleFile())

    then:
    1 * rulesFactory.createEch45ValidationRules(_) >> rules
    92 * rules[0].onElement(_)
    92 * rules[1].onElement(_)
    1 * rules[0].validate() >> []
    1 * rules[1].validate() >> []
  }

  def "it should throw a business validation exception if some rules report violations during deserialization"() {
    given:
    def rules = [Mock(StreamedValidationRule), Mock(StreamedValidationRule), Mock(StreamedValidationRule)]
    def rulesFactory = Mock(StreamedValidationRulesFactory)
    def codec = new XmlStreamingCodecFactory(rulesFactory).ech45StreamingCodec()
    rules[0].validate() >> [new Violation("rule1", "error1")]
    rules[1].validate() >> []
    rules[2].validate() >> [new Violation("rule3", "error1"), new Violation("rule3", "error2")]
    rulesFactory.createEch45ValidationRules(_) >> rules

    when:
    codec.deserialize(sampleFile())

    then:
    def exception = thrown(BusinessValidationException)
    exception.violations == [
        new Violation("rule1", "error1"), new Violation("rule3", "error1"), new Violation("rule3", "error2")
    ]
  }

  def "it should ignore disabled validation rules during deserialization"() {
    given:
    def rules = [Mock(StreamedValidationRule), Mock(StreamedValidationRule)]
    def rulesFactory = Mock(StreamedValidationRulesFactory)
    def configuration = new CodecConfiguration().withDisabledValidationRules("rule1")
    def codec = new XmlStreamingCodecFactory(rulesFactory).ech45StreamingCodec(configuration)
    rules[0].getName() >> "rule1"
    rules[0].validate() >> [new Violation("rule1", "error1")]
    rules[1].validate() >> []
    rulesFactory.createEch45ValidationRules(_) >> rules

    when:
    codec.deserialize(sampleFile())

    then:
    noExceptionThrown()
  }

  def "it should not apply business validation rules during deserialization when validation is disabled"() {
    given:
    def rulesFactory = Mock(StreamedValidationRulesFactory)
    def configuration = new CodecConfiguration().withValidationDisabled()
    def codec = new XmlStreamingCodecFactory(rulesFactory).ech45StreamingCodec(configuration)

    when:
    codec.deserialize(sampleFile())

    then:
    0 * rulesFactory.createEch45ValidationRules(_) >> []
  }

  private static XmlSource sampleFile() {
    return { getClass().getResourceAsStream(SAMPLE_FILE_PATH) }
  }
}
