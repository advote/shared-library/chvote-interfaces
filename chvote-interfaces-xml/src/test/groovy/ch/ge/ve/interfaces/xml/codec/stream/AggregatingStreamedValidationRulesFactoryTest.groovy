/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream

import ch.ge.ve.interfaces.xml.codec.stream.model.Ech222Context
import ch.ge.ve.interfaces.xml.codec.stream.model.Ech228Context
import ch.ge.ve.interfaces.xml.codec.stream.model.Ech45Context
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRule
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRulesFactory
import spock.lang.Specification

class AggregatingStreamedValidationRulesFactoryTest extends Specification {

  def rule11 = Mock(StreamedValidationRule)
  def rule12 = Mock(StreamedValidationRule)
  def rule21 = Mock(StreamedValidationRule)
  def rule22 = Mock(StreamedValidationRule)
  def factory1 = Mock(StreamedValidationRulesFactory)
  def factory2 = Mock(StreamedValidationRulesFactory)
  def factory = new AggregatingStreamedValidationRulesFactory([factory1, factory2])

  def "constructor must refuse null inputs"() {
    when:
    code.call()

    then:
    thrown(NullPointerException)

    where:
    code << [
        { _ -> new AggregatingStreamedValidationRulesFactory(null) },
        { _ -> new AggregatingStreamedValidationRulesFactory([null]) },
        { _ -> new AggregatingStreamedValidationRulesFactory([null, Mock(StreamedValidationRulesFactory)]) },
    ]
  }

  def "it should aggregate ech45 validation rules from multiple factories"() {
    given:
    def context = new Ech45Context(null, null, null, null)

    when:
    def rules = factory.createEch45ValidationRules(context)

    then:
    1 * factory1.createEch45ValidationRules(context) >> [rule11, rule12]
    1 * factory2.createEch45ValidationRules(context) >> [rule21, rule22]
    rules == [rule11, rule12, rule21, rule22]
  }

  def "it should aggregate ech222 validation rules from multiple factories"() {
    given:
    def context = new Ech222Context(null, null, null, null)

    when:
    def rules = factory.createEch222ValidationRules(context)

    then:
    1 * factory1.createEch222ValidationRules(context) >> [rule11]
    1 * factory2.createEch222ValidationRules(context) >> []
    rules == [rule11]
  }

  def "it should aggregate ech228 validation rules from multiple factories"() {
    given:
    def context = new Ech228Context(null, null, null)

    when:
    def rules = factory.createEch228ValidationRules(context)

    then:
    1 * factory1.createEch228ValidationRules(context) >> []
    1 * factory2.createEch228ValidationRules(context) >> [rule21]
    rules == [rule21]
  }
}
