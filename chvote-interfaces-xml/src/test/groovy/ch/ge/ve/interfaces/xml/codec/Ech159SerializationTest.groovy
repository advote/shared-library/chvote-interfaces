/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec

import ch.ge.ve.interfaces.xml.ech.eCH0159.v4.Delivery
import java.nio.charset.Charset
import org.junit.Assert
import org.xmlunit.builder.Input
import spock.lang.Specification

class Ech159SerializationTest extends Specification {

  private static final String SAMPLE_FILE_PATH = "/sample-eCH-0159-v4.xml"

  def factory = new XmlCodecFactory()
  def codec = factory.ech159Codec()

  def "a valid eCH-0159 file should be successfully parsed"() {
    given: 'a valid eCH-0159 XML file'
    InputStream stream = getClass().getResourceAsStream(SAMPLE_FILE_PATH)

    when:
    Delivery delivery = codec.deserialize(stream)

    then: 'the file should be correctly parsed'
    delivery != null
    delivery.deliveryHeader.senderId == "BOI://TEST_SENDER"
    delivery.deliveryHeader.messageId == "70dc0e5d947d44bdbbef2092fe993f89"

    def contest = delivery.initialDelivery.contest
    contest.contestIdentification == "201711- VP du 30 novembre 2017"
    contest.contestDescription.contestDescriptionInfo.size() == 1
    contest.contestDescription.contestDescriptionInfo[0].language == "fr"
    contest.contestDescription.contestDescriptionInfo[0].contestDescription == "VOTATION POPULAIRE Canton de GENEVE"

    def voteInformations = delivery.initialDelivery.voteInformation
    voteInformations.size() == 3
    voteInformations[0].vote.voteIdentification == "VOTATION POPULAIRE Canton de GENEVE"
    voteInformations[0].vote.domainOfInfluenceIdentification == "1"
    voteInformations[0].ballot.size() == 1
    voteInformations[0].ballot[0].ballotIdentification == "FED_CH_Question numéro 1"
    voteInformations[0].ballot[0].extension == null

    voteInformations[1].vote.voteIdentification == "201711VP-CAN-GE"
    voteInformations[1].vote.domainOfInfluenceIdentification == "3"

    voteInformations[1].ballot.size() == 1
    voteInformations[1].ballot[0].ballotIdentification == "CAN_GE_Q1"
    voteInformations[1].ballot[0].extension.groupNumberLabel == 1

    voteInformations[2].vote.voteIdentification == "201711VP-COM-6608"
    voteInformations[2].vote.domainOfInfluenceIdentification == "6608"
    voteInformations[2].ballot.size() == 1
    voteInformations[2].ballot[0].ballotIdentification == "COM_6608_Q1"
    voteInformations[2].ballot[0].extension.groupNumberLabel == 1
  }

  def "a valid delivery should be successfully serialized to xml"() {
    given: 'a delivery generated from a valid eCH-0159 file'
    Delivery delivery = codec.deserialize(getClass().getResourceAsStream(SAMPLE_FILE_PATH))

    when: 'serializing the delivery read from the eCH-0159 file'
    def xml = serializeToString(codec, delivery)

    then: 'serialization output should be similar to the original file'
    Assert.assertThat(Input.from(xml),
                      XmlMatchers.isSimilarTo(Input.fromStream(getClass().getResourceAsStream(SAMPLE_FILE_PATH))))

    and: 'output should be UTF-8-encoded'
    xml.contains('<?xml version="1.0" encoding="UTF-8"')

    and: 'ballotQuestion element content should be written as CDATA'
    xml =~ ".*<(ns.+:)?ballotQuestion><!\\[CDATA\\[.*]]>"
  }

  def "output should be optionally formatted"() {
    given: 'a delivery generated from a valid eCH-0159 file'
    Delivery delivery = codec.deserialize(getClass().getResourceAsStream(SAMPLE_FILE_PATH))
    def withFormattedOutput = new CodecConfiguration().withFormattedOutputEnabled()
    def withoutFormattedOutput = new CodecConfiguration().withFormattedOutputDisabled()

    when: 'serializing the delivery with the formatted flag on'
    def formattedXml = serializeToString(factory.ech159Codec(withFormattedOutput), delivery)

    then: 'the output should be formatted'
    def lines = formattedXml.readLines()
    lines.size() == 112
    lines[2] == '    <ns2:deliveryHeader>'

    when: 'serializing the delivery with the formatted flag off'
    def unformattedXml = serializeToString(factory.ech159Codec(withoutFormattedOutput), delivery)

    then: 'the output should not be formatted'
    unformattedXml.readLines().size() == 1
  }

  private static String serializeToString(XmlCodec<Delivery> codec, Delivery delivery) {
    def out = new ByteArrayOutputStream()
    codec.serialize(delivery, out)
    return new String(out.toByteArray(), Charset.forName("UTF-8"))
  }
}
