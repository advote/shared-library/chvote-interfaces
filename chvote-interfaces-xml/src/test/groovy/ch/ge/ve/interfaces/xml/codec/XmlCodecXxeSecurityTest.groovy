/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec

import ch.ge.ve.interfaces.xml.XmlSource
import ch.ge.ve.interfaces.xml.validation.SchemaValidationException
import spock.lang.Specification

class XmlCodecXxeSecurityTest extends Specification {

  private static final String XXED_FILE_PATH = "/sample-counting-circle-list-v1-xxe.xml"

  def factory = new XmlCodecFactory()

  def "a file containing an XXE should be deemed invalid when validation is enabled"() {
    given:
    def codec = factory.countingCircleListCodec()

    when:
    codec.deserialize(sampleXxedFile())

    then:
    thrown(SchemaValidationException)
  }

  def "a file containing an XXE should fail to be processed when validation is disabled"() {
    given:
    def codec = factory.countingCircleListCodec(new CodecConfiguration().withValidationDisabled())

    when:
    codec.deserialize(sampleXxedFile())

    then:
    thrown(XmlDeserializationException)
  }

  private static XmlSource sampleXxedFile() {
    return { getClass().getResourceAsStream(XXED_FILE_PATH) }
  }
}
