/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec

import static ch.ge.ve.interfaces.xml.util.LocalDateTimeTestUtilities.localDateTime

import ch.ge.ve.interfaces.xml.countingcircle.v1.CountingCircleListDelivery
import org.junit.Assert
import org.xmlunit.builder.Input
import spock.lang.Specification

class CountingCircleListSerializationTest extends Specification {

  private static final String SAMPLE_FILE_PATH = "/sample-counting-circle-list-v1.xml"

  def codec = new XmlCodecFactory().countingCircleListCodec()

  def "valid XML file should contain expected elements"() {
    given: 'a valid XML file'
    InputStream stream = getClass().getResourceAsStream(SAMPLE_FILE_PATH)

    when: 'unmarshalling file content'
    def delivery = codec.deserialize(stream)
    def countingCircles = delivery.countingCircleList.countingCircle

    then: 'content should be valid'
    delivery != null
    delivery.deliveryHeader.senderId == 'bo://TEST'
    delivery.deliveryHeader.messageId == '70dc0e5d947d44bdbbef2092ee473f99'
    delivery.deliveryHeader.messageDate == localDateTime('01.06.2018 09:00:00')
    delivery.deliveryHeader.sendingApplication.manufacturer == "OCSIN - SIDP"
    delivery.deliveryHeader.sendingApplication.product == "chvote-interfaces"
    delivery.deliveryHeader.sendingApplication.productVersion == "1.0"
    delivery.deliveryHeader.action == "1"
    !delivery.deliveryHeader.testDeliveryFlag

    and:
    countingCircles.size() == 1
    countingCircles[0].countingCircleId == "id"
    countingCircles[0].countingCircleName == "name"
  }

  def "a valid counting circle list delivery can be converted to xml"() {
    given: 'a valid delivery generated from an XML file'
    CountingCircleListDelivery delivery = codec.deserialize(getClass().getResourceAsStream(SAMPLE_FILE_PATH))

    when: 'marshalling'
    OutputStream out = new ByteArrayOutputStream()
    codec.serialize(delivery, out)

    then: 'content should be valid'
    Assert.assertThat(Input.from(new String(out.toByteArray(), "UTF-8")),
                      XmlMatchers.isSimilarTo(Input.fromStream(getClass().getResourceAsStream(SAMPLE_FILE_PATH))))
  }
}
