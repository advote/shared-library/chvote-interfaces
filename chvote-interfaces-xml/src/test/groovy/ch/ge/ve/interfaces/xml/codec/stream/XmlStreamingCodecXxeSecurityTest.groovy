/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream

import ch.ge.ve.interfaces.xml.XmlSource
import ch.ge.ve.interfaces.xml.codec.CodecConfiguration
import spock.lang.Specification

class XmlStreamingCodecXxeSecurityTest extends Specification {

  private static final String XXED_FILE_PATH = "/sample-eCH-0222-v1-xxe.xml"

  def factory = new XmlStreamingCodecFactory()

  def "XXE-d content should not be included in deserialized object when validation is enabled"() {
    given:
    def codec = factory.ech222StreamingCodec()

    when:
    def ech222 = codec.deserialize(sampleXxedFile())

    then:
    ech222.context.header.senderId != "xxed"
  }

  def "XXE-d content should not be included in deserialized object when validation is disabled"() {
    given:
    def codec = factory.ech222StreamingCodec(new CodecConfiguration().withValidationDisabled())

    when:
    def ech222 = codec.deserialize(sampleXxedFile())

    then:
    ech222.context.header.senderId != "xxed"
  }

  private static XmlSource sampleXxedFile() {
    return { getClass().getResourceAsStream(XXED_FILE_PATH) }
  }
}
