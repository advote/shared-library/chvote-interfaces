/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec

import ch.ge.ve.interfaces.xml.ech.eCH0157.v4.Delivery
import java.nio.charset.Charset
import org.junit.Assert
import org.xmlunit.builder.Input
import spock.lang.Specification

class Ech157SerializationTest extends Specification {

  private static final String SAMPLE_FILE_PATH = "/sample-eCH-0157-v4.xml"

  def codec = new XmlCodecFactory().ech157Codec()

  def "a valid eCH-0157 file should be successfully parsed"() {
    given: 'a valid eCH-0157 XML file'
    InputStream stream = getClass().getResourceAsStream(SAMPLE_FILE_PATH)

    when:
    Delivery delivery = codec.deserialize(stream)

    then: 'the file should be correctly parsed'
    delivery != null
    delivery.deliveryHeader.senderId == "vota3://LEGALLA"
    delivery.deliveryHeader.messageId == "e84a267a0d3848ed99883db240ec5740"

    def contest = delivery.initialDelivery.contest
    contest.contestIdentification == "205706EC"
    contest.contestDescription.contestDescriptionInfo.size() == 1
    contest.contestDescription.contestDescriptionInfo[0].language == "fr"
    contest.contestDescription.contestDescriptionInfo[0].contestDescription == "205706EC"
  }

  def "a valid delivery should be successfully serialized to xml"() {
    given: 'a delivery generated from a valid eCH-0157 file'
    Delivery delivery = codec.deserialize(getClass().getResourceAsStream(SAMPLE_FILE_PATH))

    when: 'serializing the delivery read from the eCH-0157 file'
    OutputStream out = new ByteArrayOutputStream()
    codec.serialize(delivery, out)

    then: 'serialization output should be similar to the original file'
    Assert.assertThat(Input.from(new String(out.toByteArray(), Charset.forName("UTF-8"))),
                      XmlMatchers.isSimilarTo(Input.fromStream(getClass().getResourceAsStream(SAMPLE_FILE_PATH))))
  }
}
