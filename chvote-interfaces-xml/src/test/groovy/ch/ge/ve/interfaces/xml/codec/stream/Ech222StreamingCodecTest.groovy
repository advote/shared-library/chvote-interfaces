/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.stream

import ch.ge.ve.interfaces.xml.XmlSource
import ch.ge.ve.interfaces.xml.codec.CodecConfiguration
import ch.ge.ve.interfaces.xml.codec.stream.model.CastBallot
import ch.ge.ve.interfaces.xml.validation.BusinessValidationException
import ch.ge.ve.interfaces.xml.validation.Violation
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRule
import ch.ge.ve.interfaces.xml.validation.rule.stream.StreamedValidationRulesFactory
import java.util.stream.Collectors
import spock.lang.Specification

class Ech222StreamingCodecTest extends Specification {

  private static final String SAMPLE_FILE_PATH = "/sample-eCH-0222-v1.xml"

  def "read sample ech222"() {
    given:
    def codec = new XmlStreamingCodecFactory().ech222StreamingCodec()

    when:
    def streamedEch222 = codec.deserialize(sampleFile())

    then:
    streamedEch222.context.header.messageId == "046210ff127c4cb0b00a530b844ffa93"
    streamedEch222.context.reportingBody.reportingBodyIdentification == "TBD"
    streamedEch222.context.contestIdentification == "201805 - VP du 06 mai 2018"
    streamedEch222.context.countingCircleIds.collect(Collectors.toList()) == ["6621-2105"]

    streamedEch222.stream().count() == 4
    streamedEch222.stream().map { element -> element.ballotIdentification }
                  .distinct().collect(Collectors.toList()) == ["FED_CH_Q1", "FED_CH_Q2"]
    streamedEch222.stream().map { element -> element.contestIdentification }
                  .collect(Collectors.toList()) == ["201805 - VP du 06 mai 2018"] * 4
    streamedEch222.stream().map { element -> toString(element) }.collect(Collectors.toList()) == [
        "FED_CH_Q1 : 1 - [FED_CH_Q1]",
        "FED_CH_Q2 : 2 - [FED_CH_Q1]",
        "FED_CH_Q2 : 1 - [FED_CH_Q2.a, FED_CH_Q2.b, FED_CH_Q2.c]",
        "FED_CH_Q2 : 2 - [FED_CH_Q2.a, FED_CH_Q2.b, FED_CH_Q2.c]"
    ]
  }

  def "it should apply business validation rules during deserialization when validation is enabled"() {
    given:
    def rules = [Mock(StreamedValidationRule), Mock(StreamedValidationRule)]
    def rulesFactory = Mock(StreamedValidationRulesFactory)
    def codec = new XmlStreamingCodecFactory(rulesFactory).ech222StreamingCodec()

    when:
    codec.deserialize(sampleFile())

    then:
    1 * rulesFactory.createEch222ValidationRules(_) >> rules
    4 * rules[0].onElement(_)
    4 * rules[1].onElement(_)
    1 * rules[0].validate() >> []
    1 * rules[1].validate() >> []
  }

  def "it should throw a business validation exception if some rules report violations during deserialization"() {
    given:
    def rules = [Mock(StreamedValidationRule), Mock(StreamedValidationRule), Mock(StreamedValidationRule)]
    def rulesFactory = Mock(StreamedValidationRulesFactory)
    def codec = new XmlStreamingCodecFactory(rulesFactory).ech222StreamingCodec()
    rules[0].validate() >> [new Violation("rule1", "error1")]
    rules[1].validate() >> []
    rules[2].validate() >> [new Violation("rule3", "error1"), new Violation("rule3", "error2")]
    rulesFactory.createEch222ValidationRules(_) >> rules

    when:
    codec.deserialize(sampleFile())

    then:
    def exception = thrown(BusinessValidationException)
    exception.violations == [
        new Violation("rule1", "error1"), new Violation("rule3", "error1"), new Violation("rule3", "error2")
    ]
  }

  def "it should ignore disabled validation rules during deserialization"() {
    given:
    def rules = [Mock(StreamedValidationRule), Mock(StreamedValidationRule)]
    def rulesFactory = Mock(StreamedValidationRulesFactory)
    def configuration = new CodecConfiguration().withDisabledValidationRules("rule1")
    def codec = new XmlStreamingCodecFactory(rulesFactory).ech222StreamingCodec(configuration)
    rules[0].getName() >> "rule1"
    rules[0].validate() >> [new Violation("rule1", "error1")]
    rules[1].validate() >> []
    rulesFactory.createEch222ValidationRules(_) >> rules

    when:
    codec.deserialize(sampleFile())

    then:
    noExceptionThrown()
  }

  def "it should not apply business validation rules during deserialization when validation is disabled"() {
    given:
    def rulesFactory = Mock(StreamedValidationRulesFactory)
    def configuration = new CodecConfiguration().withValidationDisabled()
    def codec = new XmlStreamingCodecFactory(rulesFactory).ech222StreamingCodec(configuration)

    when:
    codec.deserialize(sampleFile())

    then:
    0 * rulesFactory.createEch222ValidationRules(_) >> []
  }

  private static XmlSource sampleFile() {
    return { getClass().getResourceAsStream(SAMPLE_FILE_PATH) }
  }

  private static String toString(CastBallot e) {
    return "${e.ballotIdentification} : ${e.ballotCasted.ballotCastedNumber} - ${e.ballotCasted.questionRawData*.questionIdentification}"
  }
}
