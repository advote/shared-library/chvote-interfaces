/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.writer

import ch.ge.ve.interfaces.xml.ech.eCH0058.v5.HeaderType
import ch.ge.ve.interfaces.xml.ech.eCH0058.v5.SendingApplicationType
import ch.ge.ve.interfaces.xml.ech.eCH0222.v1.ReportingBodyType
import ch.ge.ve.interfaces.xml.ech.eCH0222.v1.VoteRawDataType
import ch.ge.ve.interfaces.xml.validation.SchemaValidationException
import java.nio.file.Files
import java.time.Duration
import java.time.LocalDateTime
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Ignore
import spock.lang.Specification

class Ech222WriterFactoryTest extends Specification {

  @Rule
  public TemporaryFolder temporaryFolder = new TemporaryFolder()

  def "output file should be created if it does not already exist"() {
    given:
    def folder = temporaryFolder.newFolder().toPath()
    def nonexistingOutput = folder.resolve("output.xml")
    def factory = new Ech222WriterFactory()

    when: 'creating the writer'
    def writer = factory.createWriter(nonexistingOutput)

    then: 'the output file should have been created'
    Files.exists(nonexistingOutput)

    cleanup:
    writer?.close()
  }

  def "validation can be optionally deactivated"() {
    given:
    def output = temporaryFolder.newFile().toPath()
    def validatingFactory = new Ech222WriterFactory(true)
    def novalidationFactory = new Ech222WriterFactory(false)

    when: 'writing an invalid document with validation activated'
    validatingFactory.createWriter(output).withCloseable { writer ->
      writeInvalidDocument(writer)
    }

    then:
    thrown(SchemaValidationException)

    when: 'writing the same invalid document with validation deactivated'
    novalidationFactory.createWriter(output).withCloseable { writer ->
      writeInvalidDocument(writer)
    }

    then:
    noExceptionThrown()
  }

  def writeInvalidDocument(Ech222Writer writer) {
    writer.openDocument(new HeaderType(), new ReportingBodyType(), "Test invalid document")
            .createCountingCircle("test cc 1")
            .endDocument()
  }


  @Ignore("Only executed occasionally to ensure the generation duration is reasonable")
  def "perfs - generation duration"() {
    given:
    def output = temporaryFolder.newFile().toPath()
    def factory = new Ech222WriterFactory()

    and:
    def deliveryHeader = new HeaderType(
            senderId: "Test author", messageId: "123456", messageType: "results",
            sendingApplication: new SendingApplicationType(manufacturer: "OCSIN - SIDP", product: "Test", productVersion: "1.0"),
            messageDate: LocalDateTime.now(), action: 1
    )
    def reporting = new ReportingBodyType(reportingBodyIdentification: "GE test authority", creationDateTime: LocalDateTime.now())

    and:
    // output file is about 255 Mo
    def nb_voters = 500_000

    when:
    long start = System.currentTimeMillis()
    factory.createWriter(output).withCloseable { writer ->
      def voteSectionWriter = writer.openDocument(deliveryHeader, reporting, "Test vote contest")
              .createCountingCircle("first CountingCircle sample")
              .createVoteRawData("Test Vote 1")

      generateBallots(nb_voters, "V1 - First ballot", "Q-1.1", voteSectionWriter.&appendCastBallot)
      generateBallots(nb_voters, "V1 - Second ballot", "Q-1.2", voteSectionWriter.&appendCastBallot)

      voteSectionWriter.endDocument()
    }
    def duration = Duration.ofMillis(System.currentTimeMillis() - start)

    then:
    duration < Duration.ofMinutes(1)
  }

  def generateBallots(int number, String ballotIdentification, String question, Closure consumer) {
    def random = new Random()
    number.times { i ->
      def pos = random.nextInt(3) + 1
      def ballot = new VoteRawDataType.BallotRawData(
              ballotIdentification: ballotIdentification,
              ballotCasted: new VoteRawDataType.BallotRawData.BallotCasted(
                      ballotCastedNumber: i as BigInteger,
                      questionRawData: [ new VoteRawDataType.BallotRawData.BallotCasted.QuestionRawData(
                              questionIdentification: question, casted: new VoteRawDataType.BallotRawData.BallotCasted.QuestionRawData.Casted(castedVote: pos)
                      ) ]
              )
      )

      consumer.call(ballot)
    }
  }

}
