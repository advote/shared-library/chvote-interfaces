/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.validation

import ch.ge.ve.interfaces.xml.XmlSource
import java.nio.file.Paths
import spock.lang.Specification

class ChVoteSchemaTest extends Specification {

  private static final Locale ORIGINAL_LOCALE = Locale.getDefault()

  def "closeAll method should collect all exception thrown by individual InpuStream"() {
    given: "several InputStream objects, including a 'null' one"
    def is_1 = Mock(InputStream)
    def is_2 = null
    def is_3 = Mock(InputStream)
    def is_4 = Mock(InputStream)

    and: "one of them throws an exception on call to close()"
    def onCloseException = new IOException("Fake exception thrown for test purposes")
    is_3.close() >> { throw onCloseException }

    when: "schema InputStreams are all closed"
    new ChVoteSchema().closeAll([is_1, is_2, is_3, is_4])

    then: "an IllegalStateException should be thrown"
    def ex = thrown IllegalStateException

    and: "the exception should refer all suppressed individual exceptions"
    def suppressed = ex.getSuppressed()
    suppressed.length == 2
    suppressed[0].class == NullPointerException
    suppressed[1] == onCloseException

    and: "close() should have been called on all individual InputStream objects"
    1 * is_1.close()
    // is_3 already checked by the exception verification
    1 * is_4.close()
  }

  def "validate should throw an exception if the tested file does not conform to the schema constraints"() {
    given: "an invalid eCH file"
    def source = Paths.get(getClass().getResource("/sample-invalid-eCH-0045-v4.xml").toURI())
    Locale.setDefault(Locale.ENGLISH)

    when:
    ChVoteSchema.validate(XmlSource.from(source))

    then:
    def ex = thrown SchemaValidationException
    ex.message.startsWith "Source is not XSD-valid"
    ex.cause.message.startsWith "cvc-enumeration-valid: Value 'XU' is not facet-valid with respect to enumeration"

    cleanup:
    Locale.setDefault(ORIGINAL_LOCALE)
  }

  def "validate should return normally if the tested file is OK"() {
    given: "a valid eCH file"
    def source = Paths.get(getClass().getResource("/sample-eCH-0045-v4.xml").toURI())

    when:
    ChVoteSchema.validate(XmlSource.from(source))

    then:
    noExceptionThrown()
  }

  def "validate should not take the responsibility to close the given InputStream"() {
    given:
    def realInputStream = getClass().getResourceAsStream("/sample-eCH-0045-v4.xml")
    def called = false

    def decorated = new FilterInputStream(realInputStream) {
      @Override
      void close() { called = true }
    }

    when:
    ChVoteSchema.validate(decorated)

    then:
    called == Boolean.FALSE

    cleanup:
    realInputStream.close()
  }
}
