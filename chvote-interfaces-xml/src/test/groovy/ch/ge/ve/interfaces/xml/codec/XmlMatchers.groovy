/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec

import org.xmlunit.builder.Input
import org.xmlunit.diff.Comparison
import org.xmlunit.diff.ComparisonResult
import org.xmlunit.diff.ComparisonType
import org.xmlunit.diff.DifferenceEvaluator
import org.xmlunit.diff.DifferenceEvaluators
import org.xmlunit.matchers.CompareMatcher

final class XmlMatchers {

  static CompareMatcher isSimilarTo(Input.Builder control) {
    return CompareMatcher.isSimilarTo(control).throwComparisonFailure()
                         .ignoreComments().ignoreWhitespace().normalizeWhitespace()
                         .withDifferenceEvaluator(DifferenceEvaluators.chain(DifferenceEvaluators.Default,
                                                  new XmlStandaloneEvaluator()))
  }

  private static final class XmlStandaloneEvaluator implements DifferenceEvaluator {
    @Override
    ComparisonResult evaluate(Comparison comparison, ComparisonResult outcome) {
      return (comparison.getType() == ComparisonType.XML_STANDALONE) ? ComparisonResult.SIMILAR : outcome
    }
  }
}
