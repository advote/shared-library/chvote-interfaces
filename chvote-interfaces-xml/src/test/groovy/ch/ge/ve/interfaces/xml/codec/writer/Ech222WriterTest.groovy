/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.xml.codec.writer

import ch.ge.ve.interfaces.xml.codec.XmlMatchers
import ch.ge.ve.interfaces.xml.ech.eCH0058.v5.HeaderType
import ch.ge.ve.interfaces.xml.ech.eCH0058.v5.SendingApplicationType
import ch.ge.ve.interfaces.xml.ech.eCH0222.v1.Delivery
import ch.ge.ve.interfaces.xml.ech.eCH0222.v1.ElectionRawDataType
import ch.ge.ve.interfaces.xml.ech.eCH0222.v1.ReportingBodyType
import ch.ge.ve.interfaces.xml.ech.eCH0222.v1.VoteRawDataType
import java.time.LocalDateTime
import javax.xml.bind.JAXBContext
import javax.xml.stream.XMLEventFactory
import javax.xml.stream.XMLEventWriter
import javax.xml.stream.XMLOutputFactory
import org.apache.groovy.io.StringBuilderWriter
import org.junit.Assert
import org.xmlunit.builder.Input
import spock.lang.Shared
import spock.lang.Specification

class Ech222WriterTest extends Specification {

  @Shared jaxbContext = JAXBContext.newInstance(Delivery)
  @Shared xmlEventFactory = XMLEventFactory.newInstance()
  @Shared xmlOutputFactory = XMLOutputFactory.newInstance()

  def output = new StringBuilderWriter(1024)

  private EchWriter echWriter

  void setup() {
    echWriter = new EchWriter(
            jaxbContext.createMarshaller(), xmlEventFactory, xmlOutputFactory.createXMLEventWriter(output),
            Ech222Writer.allNamespaces)
  }

  static loadResourceAsStream(String resourceName) {
    return Thread.currentThread().contextClassLoader.getResourceAsStream(resourceName)
  }


  def "write eCH-0222 for vote document"() {
    given:
    def writer = new Ech222Writer(echWriter)

    and:
    def deliveryHeader = createDeliveryHeader()
    def reporting = new ReportingBodyType(reportingBodyIdentification: "GE test authority", creationDateTime: LocalDateTime.parse("2018-11-25T09:30:08"))
    def contestIdentification = "Test vote contest"

    when:
    writer.openDocument(deliveryHeader, reporting, contestIdentification)
            .createCountingCircle("first CountingCircle sample")
            .createVoteRawData("Test Vote 1")
            .appendCastBallot(ballot("V1 - First ballot", 1, "Q-1.1", 2))
            .appendCastBallot(ballot("V1 - First ballot", 2, "Q-1.1", 1))
            .appendCastBallot(ballot("V1 - Second ballot", 1, "Q-1.2", 1))
            .appendCastBallot(ballot("V1 - Second ballot", 2, "Q-1.2", 1))
            .createNextVoteRawData("Test Vote 2")
            .appendCastBallot(ballot("V2 - First ballot", 1, "Q-2.1", 1))
            .appendCastBallot(ballot("V2 - First ballot", 2, "Q-2.1", 2))
            .endVoteRawDataSection()
            .createNextCountingCircle("second CountingCircle sample")
            .createVoteRawData("Test Vote 1")
            .appendCastBallot(ballot("V1 - First ballot", 1, "Q-1.1", 3))
            .appendCastBallot(ballot("V1 - First ballot", 2, "Q-1.1", 1))
            .endDocument()

    then:
    Assert.assertThat(Input.fromString(output.toString()),
            XmlMatchers.isSimilarTo(Input.fromStream(loadResourceAsStream("expected/sample-eCH-0222-vote.xml"))))
  }

  def "write eCH-0222 for election document"() {
    given:
    def writer = new Ech222Writer(echWriter)

    and:
    def deliveryHeader = createDeliveryHeader()
    def reporting = new ReportingBodyType(reportingBodyIdentification: "GE test authority", creationDateTime: LocalDateTime.parse("2018-11-25T09:30:08"))
    def contestIdentification = "Test election contest"

    when:
    writer.openDocument(deliveryHeader, reporting, contestIdentification)
            .createCountingCircle("MU801")
            .createElectionGroupBallotRawData("1119")
            .createElectionRawData("1119")
            .appendCastBallot(compactCastBallot())
            .appendCastBallot(emptyCastBallot())
            .appendCastBallot(scratchedCandidateCastBallot())
            .appendCastBallot(bothScratchedAndAddedCandidatesCastBallot())
            .appendCastBallot(snlCastBallot())
            .endDocument()

    then:
    Assert.assertThat(Input.fromString(output.toString()),
            XmlMatchers.isSimilarTo(Input.fromStream(loadResourceAsStream("expected/sample-eCH-0222-election.xml"))))
  }

  def "writer calls the callback after the document has been ended"() {
    given:
    def called = false
    def writer = new Ech222Writer(echWriter, { called = true })

    when:
    writer.openDocument(createDeliveryHeader(), new ReportingBodyType(), "test")

    then:
    called == Boolean.FALSE

    when:
    writer.closeDocument()

    then:
    called == Boolean.TRUE
  }

  def "close makes a call to the supplied XMLEventWriter's close method immediately"() {
    given:
    def eventFactory = Mock(XMLEventFactory)
    def xmlEventWriter = Mock(XMLEventWriter)
    def writer = new Ech222Writer(new EchWriter(jaxbContext.createMarshaller(), eventFactory, xmlEventWriter, Ech222Writer.allNamespaces))

    when:
    writer.close()

    then: 'the underlying xmlEventWriter object has been closed'
    1 * xmlEventWriter.close()

    and: 'nothing else has been done'
    0 * eventFactory._
  }

  def createDeliveryHeader() {
    return new HeaderType(
            senderId: "Test author", messageId: "444a907a7f47e559885fbe1c98addf8e620c", messageType: "results",
            sendingApplication: new SendingApplicationType(manufacturer: "OCSIN - SIDP", product: "Test", productVersion: "1.0"),
            messageDate: LocalDateTime.parse("2018-11-25T09:30:08"), action: 1
    )
  }

  // creates a BallotCasted for vote sections
  def ballot(String ballotIdentification, int number, String question, int pos) {
    return new VoteRawDataType.BallotRawData(
            ballotIdentification: ballotIdentification,
            ballotCasted: new VoteRawDataType.BallotRawData.BallotCasted(
                    ballotCastedNumber: number as BigInteger,
                    questionRawData: [ new VoteRawDataType.BallotRawData.BallotCasted.QuestionRawData(
                            questionIdentification: question, casted: new VoteRawDataType.BallotRawData.BallotCasted.QuestionRawData.Casted(castedVote: pos)
                    ) ]
            )
    )
  }

  def compactCastBallot() {
    return new ElectionRawDataType.BallotRawData(
            listRawData: new ElectionRawDataType.BallotRawData.ListRawData(listIdentification: "01"),
            ballotPosition: [
                    positionWithCandidate("2776", "0101"),
                    positionWithCandidate("2797", "0102"),
                    positionWithCandidate("2777", "0103"),
                    positionWithCandidate("1089", "0104"),
                    positionWithCandidate("2778", "0105")
            ],
            isUnchangedBallot: true
    )
  }

  def emptyCastBallot() {
    return new ElectionRawDataType.BallotRawData(
            listRawData: new ElectionRawDataType.BallotRawData.ListRawData(listIdentification: "99"),
            ballotPosition: [positionEmpty()] * 5,
            isUnchangedBallot: true
    )
  }

  def scratchedCandidateCastBallot() {
    return new ElectionRawDataType.BallotRawData(
            listRawData: new ElectionRawDataType.BallotRawData.ListRawData(listIdentification: "01"),
            ballotPosition: [
                    positionEmpty(),
                    positionWithCandidate("2797", "0102"),
                    positionWithCandidate("2777", "0103"),
                    positionWithCandidate("1089", "0104"),
                    positionWithCandidate("2778", "0105")
            ],
            isUnchangedBallot: false
    )
  }

  def bothScratchedAndAddedCandidatesCastBallot() {
    return new ElectionRawDataType.BallotRawData(
            listRawData: new ElectionRawDataType.BallotRawData.ListRawData(listIdentification: "01"),
            ballotPosition: [
                    positionWithCandidate("2797", "0102"),
                    positionWithCandidate("2201", "0201"),
                    positionWithCandidate("2777", "0103"),
                    positionWithCandidate("1089", "0104"),
                    positionWithCandidate("2778", "0105")
            ],
            isUnchangedBallot: false
    )
  }

  def snlCastBallot() {
    return new ElectionRawDataType.BallotRawData(
            listRawData: new ElectionRawDataType.BallotRawData.ListRawData(listIdentification: "99"),
            ballotPosition: [
                    positionWithCandidate("2776", "0101"),
                    positionWithCandidate("2201", "0201"),
                    positionWithCandidate("747", "0301"),
                    positionWithCandidate("2401", "0401"),
                    positionWithCandidate("2768", "0501")
            ],
            isUnchangedBallot: false
    )
  }

  def positionEmpty() {
    return new ElectionRawDataType.BallotRawData.BallotPosition(isEmpty: true)
  }

  def positionWithCandidate(String candidateId, String candidateRef) {
    return new ElectionRawDataType.BallotRawData.BallotPosition(
            candidate: new ElectionRawDataType.BallotRawData.BallotPosition.Candidate(
                    candidateIdentification: candidateId,
                    candidateReferenceOnPosition: candidateRef
            )
    )
  }
}
